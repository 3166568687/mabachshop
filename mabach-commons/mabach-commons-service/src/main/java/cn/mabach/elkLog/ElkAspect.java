//package cn.mabach.elkLog;
//
//
//
//import cn.mabach.elkLog.AsyncMethod;
//import lombok.extern.slf4j.Slf4j;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Pointcut;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import org.springframework.web.context.request.RequestContextHolder;
//import org.springframework.web.context.request.ServletRequestAttributes;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.Arrays;
//import java.util.List;
//
//@Aspect
//@Component
//@Slf4j
//public class ElkAspect {
//    @Autowired
//    private AsyncMethod asyncMethod;
//
//    @Pointcut("execution(* cn.mabach.*.service.*.*(..))")
//    public void logPointCut(){
//
//    }
//
//    @Around(value = "logPointCut()")
//    public Object aroud(ProceedingJoinPoint point) throws Throwable {
//
//
//        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
//        String signature = point.getSignature().toShortString();
//        Object[] pointArgs = point.getArgs();
//        List<Object> args = Arrays.asList(pointArgs);
//        long s = System.currentTimeMillis();
//        Object proceed = point.proceed();
//        long e = System.currentTimeMillis();
//        Long time=e-s;
//
//        asyncMethod.sendLog(request,args.toString(),time,proceed,signature);
//        return proceed;
//
//    }
//
//
//
//
//
//
//}
