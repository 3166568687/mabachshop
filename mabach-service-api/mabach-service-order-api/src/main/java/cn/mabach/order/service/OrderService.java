package cn.mabach.order.service;


import cn.mabach.order.dto.OrderDeliveryParam;
import cn.mabach.order.dto.OrderDetail;
import cn.mabach.order.entity.OrderEntity;
import cn.mabach.order.vo.MoneyInfoParam;
import cn.mabach.order.vo.ReceiverInfoParam;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
/**
 * 订单表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-02 23:46:11
 */
@FeignClient(name = "app-mabach-order")
@Api(tags = "OrderService", description = "订单管理")
public interface OrderService   {

    @GetMapping("/listOrder")
    @ApiOperation(value = "分页查询")
    RS<PageResult<OrderEntity>> queryPage(@RequestParam(value = "orderSn",required = false) String orderSn,
                                          @RequestParam(value = "receiverKeyword",required = false) String receiverKeyword,
                                          @RequestParam(value = "status",required = false) Integer status,
                                          @RequestParam(value = "orderType",required = false) Integer orderType,
                                          @RequestParam(value = "sourceType",required = false) Integer sourceType,
                                          @RequestParam(value = "createTime",required = false) Date createTime,
                                          @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                          @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoOrder")
    @ApiOperation(value = "根据id查找")
    RS<OrderDetail> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveOrder")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody OrderEntity entity);

    @PostMapping("/updateOrder")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody OrderEntity entity);

    @PostMapping("/deleteOrders")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);

    @ApiOperation("单个删除")
    @RequestMapping(value = "/deleteOrderByid", method = RequestMethod.POST)
    public RS removeByIdE(@RequestParam("id") Long id) ;

    @ApiOperation("批量发货")
    @RequestMapping(value = "/updateDelivery", method = RequestMethod.POST)
    public RS delivery(@RequestBody OrderDeliveryParam param) ;

    @ApiOperation("批量关闭订单")
    @RequestMapping(value = "/updateCloseStatus", method = RequestMethod.POST)
    public RS close(Long id);

    @ApiOperation("修改收货人信息")
    @RequestMapping(value = "/updateReceiverInfo", method = RequestMethod.POST)
    public RS updateReceiverInfo(@RequestBody ReceiverInfoParam receiverInfoParam);

    @ApiOperation("修改订单费用信息")
    @RequestMapping(value = "/updateMoneyInfo", method = RequestMethod.POST)
    public RS updateMoneyInfo(@RequestBody MoneyInfoParam moneyInfoParam);

    @ApiOperation("备注订单")
    @RequestMapping(value = "/updateNote", method = RequestMethod.POST)
    public RS updateNote(@RequestParam("id") Long id,
                         @RequestParam("note") String note);
}

