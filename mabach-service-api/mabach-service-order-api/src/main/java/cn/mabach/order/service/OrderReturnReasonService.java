package cn.mabach.order.service;


import cn.mabach.order.entity.OrderReturnReasonEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 退货原因表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-02 23:46:11
 */
@FeignClient(name = "app-mabach-order")
@Api(tags = "OrderReturnReasonService", description = "退货原因管理")
public interface OrderReturnReasonService   {

    @GetMapping("/listOrderReturnReason")
    @ApiOperation(value = "分页查询")
    RS<PageResult<OrderReturnReasonEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                      @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                      @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoOrderReturnReason")
    @ApiOperation(value = "根据id查找")
    RS<OrderReturnReasonEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveOrderReturnReason")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody OrderReturnReasonEntity entity);

    @PostMapping("/updateOrderReturnReason")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody OrderReturnReasonEntity entity);

    @PostMapping("/deleteOrderReturnReasons")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);

    @ApiOperation("单个删除")
    @RequestMapping(value = "/deleteOrderReturnReasonByid", method = RequestMethod.POST)
    public RS removeByIdE(@RequestParam("id") Long id) ;

    @ApiOperation("修改退货原因启用状态")
    @RequestMapping(value = "/updateOrderReturnReasonEntityStatus", method = RequestMethod.POST)
    public RS updateStatus(@RequestParam(value = "status") Integer status,
                           @RequestParam("ids") List<Long> ids) ;
}

