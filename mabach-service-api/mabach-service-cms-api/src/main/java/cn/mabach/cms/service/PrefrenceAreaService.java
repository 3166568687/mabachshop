package cn.mabach.cms.service;



import cn.mabach.cms.entity.PrefrenceAreaEntity;
import cn.mabach.result.RS;
import cn.mabach.result.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 优选专区
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-02-13 22:18:24
 */
@Api(tags = "广告服务接口")
public interface PrefrenceAreaService   {

    @GetMapping("/listPrefrenceArea")
    @ApiOperation(value = "分页查询")
    TableDataInfo queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoPrefrenceArea")
    @ApiOperation(value = "根据id查找")
    RS<PrefrenceAreaEntity> getByIdE(@RequestParam("id") Long id);


    @PostMapping("/savePrefrenceArea")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody PrefrenceAreaEntity entity);


    @PutMapping("/updatePrefrenceArea")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody PrefrenceAreaEntity entity);

    @DeleteMapping("/deletePrefrenceAreas")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);


}

