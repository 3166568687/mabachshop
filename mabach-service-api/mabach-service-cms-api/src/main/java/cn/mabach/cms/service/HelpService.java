package cn.mabach.cms.service;


import cn.mabach.cms.entity.HelpEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 帮助表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-27 20:52:17
 */
@Api(tags = "广告服务接口")
@RequestMapping("/")
@FeignClient(name="app-mabach-cms")
public interface HelpService   {

    @GetMapping("/listHelp")
    @ApiOperation(value = "分页查询")
    RS<PageResult<HelpEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                         @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                         @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoHelp")
    @ApiOperation(value = "根据id查找")
    RS<HelpEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveHelp")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody HelpEntity entity);

    @PostMapping("/updateHelp")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody HelpEntity entity);

    @PostMapping("/deleteOneHelp")
    @ApiOperation(value = "根据ID删除")
    RS removeOneById(@RequestParam("id") Long id);

    @PostMapping("/deleteHelp")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestBody List<Long> ids);
}

