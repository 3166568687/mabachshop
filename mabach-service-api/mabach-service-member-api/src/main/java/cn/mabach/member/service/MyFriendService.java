package cn.mabach.member.service;


import cn.mabach.member.entity.MyFriendEntity;
import cn.mabach.result.TableDataInfo;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-02-24 21:58:59
 */
@Api(tags = "广告服务接口")
public interface MyFriendService   {

    @GetMapping("/listMyFriend")
    @ApiOperation(value = "分页查询")
    TableDataInfo queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoMyFriend")
    @ApiOperation(value = "根据id查找")
    RS<MyFriendEntity> getByIdE(@RequestParam("id") Long id);


    @PostMapping("/saveMyFriend")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody MyFriendEntity entity);


    @PutMapping("/updateMyFriend")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody MyFriendEntity entity);

    @DeleteMapping("/deleteMyFriends")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);


}

