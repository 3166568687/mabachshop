package cn.mabach.member.service;


import cn.mabach.member.entity.MemberRuleSettingEntity;
import cn.mabach.result.RS;
import cn.mabach.result.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 会员积分成长规则表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-04 17:30:47
 */
@Api(tags = "会员规则")
public interface MemberRuleSettingService   {

    @GetMapping("/listMemberRuleSetting")
    @ApiOperation(value = "分页查询")
    TableDataInfo queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoMemberRuleSetting")
    @ApiOperation(value = "根据id查找")
    RS<MemberRuleSettingEntity> getByIdE(@RequestParam("id") Long id);


    @PostMapping("/saveMemberRuleSetting")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody MemberRuleSettingEntity entity);


    @PutMapping("/updateMemberRuleSetting")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody MemberRuleSettingEntity entity);

    @DeleteMapping("/deleteMemberRuleSettings")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);


}

