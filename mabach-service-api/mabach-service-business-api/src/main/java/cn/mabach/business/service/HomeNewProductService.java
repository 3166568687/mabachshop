package cn.mabach.business.service;


import cn.mabach.business.entity.HomeNewProductEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 新鲜好物表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 21:38:07
 */
@Api(tags = "HomeNewProductService", description = "首页新品管理")
public interface HomeNewProductService   {

    @GetMapping("/listHomeNewProduct")
    @ApiOperation(value = "分页查询")
    RS<PageResult<HomeNewProductEntity>> queryPage(@RequestParam(value = "productName", required = false) String productName,
                                                   @RequestParam(value = "recommendStatus", required = false) Integer recommendStatus,
                                                   @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                   @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoHomeNewProduct")
    @ApiOperation(value = "根据id查找")
    RS<HomeNewProductEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveHomeNewProduct")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody HomeNewProductEntity entity);

    @PostMapping("/updateHomeNewProduct")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody HomeNewProductEntity entity);

    @PostMapping("/deleteHomeNewProducts")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);

    @ApiOperation("单个删除")
    @RequestMapping(value = "/deleteHomeNewProductByid", method = RequestMethod.POST)
    public RS removeByIdE(@RequestParam("id") Long id) ;

    @ApiOperation("修改推荐排序")
    @RequestMapping(value = "/updateHomeNewProductSortByid", method = RequestMethod.POST)
    public RS updateSort(@RequestParam("id") Long id, @RequestParam("sort") Integer sort);

    @ApiOperation("批量修改推荐状态")
    @RequestMapping(value = "/updateHomeNewProductRecommendStatus", method = RequestMethod.POST)
    public RS updateRecommendStatus(@RequestParam("ids") List<Long> ids, @RequestParam Integer recommendStatus) ;
}

