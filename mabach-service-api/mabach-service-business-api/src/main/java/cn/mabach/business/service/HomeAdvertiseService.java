package cn.mabach.business.service;


import cn.mabach.business.entity.HomeAdvertiseEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 首页轮播广告表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 21:38:07
 */
@Api(tags = "HomeAdvertiseService", description = "首页轮播广告管理")
public interface HomeAdvertiseService   {

    @GetMapping("/listHomeAdvertise")
    @ApiOperation(value = "分页查询")
    RS<PageResult<HomeAdvertiseEntity>> queryPage(@RequestParam(value = "name", required = false) String name,
                                                  @RequestParam(value = "type", required = false) Integer type,
                                                  @RequestParam(value = "endTime", required = false) String endTime,
                                                  @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                  @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoHomeAdvertise")
    @ApiOperation(value = "根据id查找")
    RS<HomeAdvertiseEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveHomeAdvertise")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody HomeAdvertiseEntity entity);

    @PostMapping("/updateHomeAdvertise")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody HomeAdvertiseEntity entity);

    @PostMapping("/deleteHomeAdvertises")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);

    @ApiOperation("单个删除")
    @RequestMapping(value = "/deleteHomeAdvertiseByid", method = RequestMethod.POST)
    public RS removeByIdE(@RequestParam("id") Long id) ;

    @ApiOperation("修改上下线状态")
    @RequestMapping(value = "/updateHomeAdvertiseStatusByid", method = RequestMethod.POST)
    public RS updateStatus(@RequestParam("id") Long id, @RequestParam("status") Integer status) ;
}

