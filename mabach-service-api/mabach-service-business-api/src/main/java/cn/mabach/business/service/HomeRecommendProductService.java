package cn.mabach.business.service;


import cn.mabach.business.entity.HomeRecommendProductEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 人气推荐商品表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 21:38:43
 */
@Api(tags = "HomeRecommendProductService", description = "首页人气推荐管理")
public interface HomeRecommendProductService   {

    @GetMapping("/listHomeRecommendProduct")
    @ApiOperation(value = "分页查询")
    RS<PageResult<HomeRecommendProductEntity>> queryPage(@RequestParam(value = "productName", required = false) String productName,
                                                         @RequestParam(value = "recommendStatus", required = false) Integer recommendStatus,
                                                         @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                         @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoHomeRecommendProduct")
    @ApiOperation(value = "根据id查找")
    RS<HomeRecommendProductEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveHomeRecommendProduct")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody HomeRecommendProductEntity entity);

    @PostMapping("/updateHomeRecommendProduct")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody HomeRecommendProductEntity entity);

    @PostMapping("/deleteHomeRecommendProducts")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);

    @ApiOperation("单个删除")
    @RequestMapping(value = "/deleteHomeRecommendProductByid", method = RequestMethod.POST)
    public RS removeByIdE(@RequestParam("id") Long id) ;

    @ApiOperation("修改推荐排序")
    @RequestMapping(value = "/updateHomeRecommendProductSort", method = RequestMethod.POST)
    public RS updateSort(@RequestParam("id") Long id, @RequestParam("sort") Integer sort);

    @ApiOperation("批量修改推荐状态")
    @RequestMapping(value = "/updateHomeRecommendProductStatus", method = RequestMethod.POST)
    public RS updateRecommendStatus(@RequestParam("ids") List<Long> ids, @RequestParam("recommendStatus") Integer recommendStatus) ;
}

