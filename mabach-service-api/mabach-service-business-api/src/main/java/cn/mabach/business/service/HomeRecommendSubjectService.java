package cn.mabach.business.service;


import cn.mabach.business.entity.HomeRecommendSubjectEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * 首页推荐专题表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 21:38:43
 */

@Api(tags = "HomeRecommendSubjectService", description = "首页专题推荐管理")
public interface HomeRecommendSubjectService   {

    @GetMapping("/listHomeRecommendSubject")
    @ApiOperation(value = "分页查询")
    RS<PageResult<HomeRecommendSubjectEntity>> queryPage(@RequestParam(value = "subjectName", required = false) String subjectName,
                                                         @RequestParam(value = "recommendStatus", required = false) Integer recommendStatus,
                                                         @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                         @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoHomeRecommendSubject")
    @ApiOperation(value = "根据id查找")
    RS<HomeRecommendSubjectEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveHomeRecommendSubject")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody HomeRecommendSubjectEntity entity);

    @PostMapping("/updateHomeRecommendSubject")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody HomeRecommendSubjectEntity entity);

    @PostMapping("/deleteHomeRecommendSubjects")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);

    @ApiOperation("单个删除")
    @RequestMapping(value = "/deleteHomeRecommendSubjectByid", method = RequestMethod.POST)
    public RS removeByIdE(@RequestParam("id") Long id) ;

    @ApiOperation("修改推荐排序")
    @RequestMapping(value = "/updateHomeRecommendSubjectSort", method = RequestMethod.POST)
    public RS updateSort(@RequestParam("id") Long id, @RequestParam("sort") Integer sort);

    @ApiOperation("批量修改推荐状态")
    @RequestMapping(value = "/updateHomeRecommendSubjectRecommendStatusByids", method = RequestMethod.POST)
    public RS updateRecommendStatus(@RequestParam("ids") List<Long> ids, @RequestParam Integer recommendStatus);
}

