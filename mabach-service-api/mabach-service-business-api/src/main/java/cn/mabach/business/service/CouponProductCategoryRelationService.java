package cn.mabach.business.service;


import cn.mabach.business.entity.CouponProductCategoryRelationEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.List;
/**
 * 优惠券和产品分类关系表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 17:40:52
 */
@Api(tags = "广告服务接口")
public interface CouponProductCategoryRelationService   {

    @GetMapping("/listCouponProductCategoryRelation")
    @ApiOperation(value = "分页查询")
    RS<PageResult<CouponProductCategoryRelationEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                                  @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                                  @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);

    @GetMapping("/infoCouponProductCategoryRelationCouponId")
    @ApiOperation(value = "根据id查找")
    RS<List<CouponProductCategoryRelationEntity>> getByCouponId(Long id);

    @GetMapping("/infoCouponProductCategoryRelation")
    @ApiOperation(value = "根据id查找")
    RS<CouponProductCategoryRelationEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveCouponProductCategoryRelation")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody CouponProductCategoryRelationEntity entity);

    @PostMapping("/updateCouponProductCategoryRelation")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody CouponProductCategoryRelationEntity entity);

    @PostMapping("/deleteCouponProductCategoryRelation")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestParam("ids") List<Long> ids);

    @PostMapping("/deleteCouponProductCategoryRelationByCouponId")
    @ApiOperation(value = "根据优惠券ID删除")
    RS deleteByCouponId(Long id);
}

