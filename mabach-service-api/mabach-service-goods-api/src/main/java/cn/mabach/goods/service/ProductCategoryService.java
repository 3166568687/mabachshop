package cn.mabach.goods.service;

import cn.mabach.goods.entity.ProductCategoryEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * 产品分类
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-26 01:03:48
 */
@Api(tags = "PmsProductCategoryController", description = "商品分类管理")
public interface ProductCategoryService   {

    @GetMapping("/listProductCategory")
    @ApiOperation(value = "分页查询")
    RS<PageResult<ProductCategoryEntity>> queryPage(@RequestParam(value = "parentId") Long parentId,
                                                    @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                    @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoProductCategory")
    @ApiOperation(value = "根据id查找")
    RS<ProductCategoryEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveProductCategory")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody ProductCategoryEntity entity);

    @PostMapping("/updateProductCategory")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody ProductCategoryEntity entity);

    @PostMapping("/deleteOneProductCategory")
    @ApiOperation(value = "根据ID删除")
    RS removeOneById(@RequestParam("id") Long id);

    @PostMapping("/deleteProductCategory")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestBody List<Long> ids);

    @ApiOperation("修改导航栏显示状态")
    @PostMapping(value = "/update/navStatusProductCategory")
    RS updateNavStatus(@RequestParam("ids") Long ids, @RequestParam("navStatus") Integer navStatus);

    @ApiOperation("修改显示状态")
    @PostMapping(value = "/update/showStatusProductCategory")
    public RS updateShowStatus(@RequestParam("ids") Long ids, @RequestParam("showStatus") Integer showStatus);

    @ApiOperation("查询所有一级分类及子分类")
    @GetMapping(value = "/list/withChildrenProductCategory")
    public RS<List<Map>> listWithChildren() ;
}

