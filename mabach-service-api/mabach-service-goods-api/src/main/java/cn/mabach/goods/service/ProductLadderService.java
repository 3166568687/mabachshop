package cn.mabach.goods.service;

import cn.mabach.goods.entity.ProductLadderEntity;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 产品阶梯价格表(只针对同商品)
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-26 01:03:48
 */
@Api(tags = "广告服务接口")
public interface ProductLadderService   {

    @GetMapping("/listProductLadder")
    @ApiOperation(value = "分页查询")
    RS<PageResult<ProductLadderEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                  @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                  @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);



    @GetMapping("/infoProductLadder")
    @ApiOperation(value = "根据id查找")
    RS<ProductLadderEntity> getByIdE(@RequestParam("id") Long id);

    @PostMapping("/saveProductLadder")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody ProductLadderEntity entity);

    @PostMapping("/updateProductLadder")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody ProductLadderEntity entity);

    @PostMapping("/deleteOneProductLadder")
    @ApiOperation(value = "根据ID删除")
    RS removeOneById(@RequestParam("id") Long id);

    @PostMapping("/deleteProductLadder")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestBody List<Long> ids);
}

