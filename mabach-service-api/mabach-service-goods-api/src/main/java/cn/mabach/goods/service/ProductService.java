package cn.mabach.goods.service;


import cn.mabach.goods.dto.ProductParam;
import cn.mabach.goods.dto.ProductResult;
import cn.mabach.goods.entity.ProductEntity;
import cn.mabach.goods.vo.ProductQueryParam;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * 商品信息
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-26 01:03:48
 */
@Api(tags = "ProductService", description = "商品管理")
public interface ProductService   {

    @PostMapping("/listProduct")
    @ApiOperation(value = "分页查询")
    RS<PageResult<ProductEntity>> queryPage(ProductQueryParam productQueryParam,
                                            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                            @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize);

    @GetMapping("/infoProduct")
    @ApiOperation(value = "根据id查找")
    RS<ProductResult> getByIdE(@RequestParam("id") Long id);

    @GetMapping("/infoProductOne")
    @ApiOperation(value = "根据id查找原始商品")
    RS<ProductEntity> getOneById(@RequestParam("id") Long id);

    @PostMapping("/saveProduct")
    @ApiOperation("创建商品")
    RS saveE(@RequestBody ProductParam entity) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException;

    @PostMapping("/updateProduct")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody ProductParam entity) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException;





//    @ApiOperation("根据商品名称或货号模糊查询")
//    @RequestMapping(value = "/simpleList", method = RequestMethod.GET)
//    public RS<List<ProductEntity>> getList(String keyword) ;

    @ApiOperation("批量修改审核状态")
    @PostMapping(value = "/update/verifyStatus")
    public RS updateVerifyStatus(@RequestParam("id") Long id,
                                 @RequestParam("verifyStatus") Integer verifyStatus) ;
    @ApiOperation("批量上下架")
    @PostMapping(value = "/update/publishStatus")
    public RS updatePublishStatus(@RequestParam("ids") List<Long> ids,
                                  @RequestParam("publishStatus") Integer publishStatus);

    @ApiOperation("批量推荐商品")
    @PostMapping(value = "/update/recommendStatus")
    public RS updateRecommendStatus(@RequestParam("ids") List<Long> ids,
                                    @RequestParam("recommendStatus") Integer recommendStatus);


    /**
     * 批量设为新品
     */
    @ApiOperation("批量设为新品")
    @PostMapping(value = "/update/newStatus")
    public RS updateNewStatus(@RequestParam("ids") List<Long> ids,
                              @RequestParam("newStatus") Integer newStatus) ;

    /**
     * 批量逻辑删除
     */
    @ApiOperation("批量逻辑删除")
    @PostMapping(value = "/update/deleteStatus")
    public RS updateDeleteStatus(@RequestParam("ids") List<Long> ids,
                                 @RequestParam("deleteStatus") Integer deleteStatus);


}

