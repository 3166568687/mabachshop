package cn.mabach.dianping.service;

import cn.mabach.dianping.dto.ShopDto;
import cn.mabach.dianping.entity.ShopEntity;
import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import com.sun.javaws.exceptions.InvalidArgumentException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-02-28 18:02:45
 */
@Api("ShopService")
public interface ShopService {

//    @GetMapping("/initShopList")
//    public String init();


    @GetMapping("/log2")
    public String printlog2() throws InterruptedException;

    @GetMapping("/findShopList")
    @ApiOperation(value = "分页查询")
    public PageResult queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);

    @GetMapping("/infoShop")
    @ApiOperation(value = "根据id查找")
    ShopEntity getByIdE(@RequestParam("id") Integer id);

    @PostMapping("/saveShop")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody ShopEntity entity);

    @PostMapping("/updateShop")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody ShopEntity entity);

    @PostMapping("/deleteOneShop")
    @ApiOperation(value = "根据ID删除")
    RS removeOneById(@RequestParam("id") Integer id);

    @PostMapping("/deleteShops")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestBody List<Integer> ids);

    @GetMapping("/recommendShop")
    RS<List<ShopDto>> recommend(  @RequestParam("uid") String uid) throws IOException;

    @GetMapping("/searchShop")
    Map<String,Object> search(@RequestParam(value = "longitude") BigDecimal longitude,
                              @RequestParam(value = "latitude") BigDecimal latitude,
                              @RequestParam("keyword") String keyword,
                              @RequestParam(value = "orderby",required = false)String orderby,
                              @RequestParam(value = "categoryId",required = false)Integer categoryId,
                              @RequestParam(value = "tags",required = false)String tags) throws IOException;
}

