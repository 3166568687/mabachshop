package cn.mabach.dianping.dto;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-02-28 20:06:25
 */
@Data
@EqualsAndHashCode
public class ShopDto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */

	private Integer id;

	private String name;
	/**
	 * 
	 */
	private BigDecimal remarkScore;
	/**
	 * 
	 */
	private Integer pricePerMan;

	/**
	 * 
	 */
	private Integer categoryId;
	/**
	 * 
	 */
	private String tags;

	private String logo;

	private Integer distance;
	private String hot;
	private String category;

}
