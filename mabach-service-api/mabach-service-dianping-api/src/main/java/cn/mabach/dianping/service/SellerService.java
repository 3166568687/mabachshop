package cn.mabach.dianping.service;

import cn.mabach.dianping.entity.SellerEntity;
import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.List;

/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-02-28 16:53:43
 */
@Api(tags = "SellerService", description = "商家管理")
public interface SellerService  {
    @GetMapping("/findSellerList")
    @ApiOperation(value = "分页查询")
    public PageResult queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize);

    @GetMapping("/infoSeller")
    @ApiOperation(value = "根据id查找")
    SellerEntity getByIdE(@RequestParam("id") Integer id);

    @PostMapping("/saveSeller")
    @ApiOperation(value = "保存")
    RS saveE(@RequestBody SellerEntity entity);

    @PostMapping("/updateSeller")
    @ApiOperation(value = "修改")
    RS updateByIdE(@RequestBody SellerEntity entity);

    @PostMapping("/deleteOneSeller")
    @ApiOperation(value = "根据ID删除")
    RS removeOneById(@RequestParam("id") Integer id);

    @PostMapping("/deleteSellers")
    @ApiOperation(value = "根据ID集合删除")
    RS removeByIdsE(@RequestBody List<Integer> ids);

    @PostMapping("/updateStatus")
    @ApiOperation(value = "更改禁用状态")
    public RS updateStatus(@RequestParam("id") Integer id,@RequestParam("disabledFlag")Integer disabledFlag);

}

