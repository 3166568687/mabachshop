import http from '@/utils/httpRequest'
export function fetchList(params) {
  return http({
    url:http.adornUrl('/flashProductRelation/list'),
    method:'get',
    params:params
  })
}
export function createFlashProductRelation(data) {
  return http({
    url:http.adornUrl('/flashProductRelation/create'),
    method:'post',
    data:data
  })
}
export function deleteFlashProductRelation(id) {
  return http({
    url:http.adornUrl('/flashProductRelation/delete/'+id),
    method:'post'
  })
}
export function updateFlashProductRelation(id,data) {
  return http({
    url:http.adornUrl('/flashProductRelation/update/'+id),
    method:'post',
    data:data
  })
}
