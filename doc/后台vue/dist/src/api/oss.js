import http from '@/utils/httpRequest'
export function policy() {
  return http({
    url:http.adornUrl('/aliyun/oss/policy'),
    method:'get',
  })
}
