import http from '@/utils/httpRequest'
export function fetchList(params) {
  return http({
    url:http.adornUrl('/flash/list'),
    method:'get',
    params:params
  })
}
export function updateStatus(id,params) {
  return http({
    url:http.adornUrl('/flash/update/status/'+id),
    method:'post',
    params:params
  })
}
export function deleteFlash(id) {
  return http({
    url:http.adornUrl('/flash/delete/'+id),
    method:'post'
  })
}
export function createFlash(data) {
  return http({
    url:http.adornUrl('/flash/create'),
    method:'post',
    data:data
  })
}
export function updateFlash(id,data) {
  return http({
    url:http.adornUrl('/flash/update/'+id),
    method:'post',
    data:data
  })
}
