import http from '@/utils/httpRequest'
export function fetchList(params) {
  return http({
    url:http.adornUrl('/seller/list'),
    method:'get',
    params:params
  })
}
export function createBrand(data) {
  return http({
    url:http.adornUrl('/seller/save'),
    method:'post',
    data:data
  })
}
export function updateShowStatus(data) {
  return http({
    url:http.adornUrl('/seller/update/showStatus'),
    method:'post',
    params:data 
  })
}



export function deleteBrand(id) {
  return http({
    url:http.adornUrl('/seller/delete/'+id),
    method:'get',
  })
}

export function getBrand(id) {
  return http({
    url:http.adornUrl('/seller/'+id),
    method:'get',
  })
}

export function updateBrand(id,data) {
  return http({
    url:http.adornUrl('/seller/update/'+id),
    method:'post',
    data:data
  })
}

