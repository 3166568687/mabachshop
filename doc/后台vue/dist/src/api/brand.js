import http from '@/utils/httpRequest'
export function fetchList(params) {
  return http({
    url:http.adornUrl('/brand/list'),
    method:'get',
    params:params
  })
}
export function createBrand(data) {
  return http({
    url:http.adornUrl('/brand/create'),
    method:'post',
    data:data
  })
}
export function updateShowStatus(data) {
  return http({
    url:http.adornUrl('/brand/update/showStatus'),
    method:'post',
    params:data
  })
}

export function updateFactoryStatus(data) {
  return http({
    url:http.adornUrl('/brand/update/factoryStatus'),
    method:'post',
    params:data
  })
}

export function deleteBrand(id) {
  return http({
    url:http.adornUrl('/brand/delete/'+id),
    method:'get',
  })
}

export function getBrand(id) {
  return http({
    url:http.adornUrl('/brand/'+id),
    method:'get',
  })
}

export function updateBrand(id,data) {
  return http({
    url:http.adornUrl('/brand/update/'+id),
    method:'post',
    data:data
  })
}

