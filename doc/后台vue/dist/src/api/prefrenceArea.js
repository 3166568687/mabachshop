import http from '@/utils/httpRequest'
export function fetchList() {
  return http({
    url:http.adornUrl('/prefrenceArea/listAll'),
    method:'get',
  })
}
