
import http from '@/utils/httpRequest'
export function fetchList(params) {
  return http({
    url:http.adornUrl('/product/list'),
    method:'get',
    params:params
  })
}

export function fetchSimpleList(params) {
  return http({
    url:http.adornUrl('/product/simpleList'),
    method:'get',
    params:params
  })
}

export function updateDeleteStatus(params) {
  return http({
    url:http.adornUrl('/product/update/deleteStatus'),
    method:'post',
    params:params
  })
}

export function updateNewStatus(params) {
  return http({
    url:http.adornUrl('/product/update/newStatus'),
    method:'post',
    params:params
  })
}

export function updateRecommendStatus(params) {
  return http({
    url:http.adornUrl('/product/update/recommendStatus'),
    method:'post',
    params:params
  })
}

export function updatePublishStatus(params) {
  return http({
    url:http.adornUrl('/product/update/publishStatus'),
    method:'post',
    params:params
  })
}

export function createProduct(data) {
  return http({
    url:http.adornUrl('/product/create'),
    method:'post',
    data:data
  })
}

export function updateProduct(id,data) {
  return http({
    url:http.adornUrl('/product/update/'+id),
    method:'post',
    data:data
  })
}

export function getProduct(id) {
  return http({
    url:http.adornUrl('/product/updateInfo/'+id),
    method:'get',
  })
}

