import http from '@/utils/httpRequest'
export function fetchListAll() {
  return http({
    url:http.adornUrl('/subject/listAll'),
    method:'get',
  })
}

export function fetchList(params) {
  return http({
    url:http.adornUrl('/subject/list'),
    method:'get',
    params:params
  })
}
