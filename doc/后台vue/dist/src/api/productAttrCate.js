import http from '@/utils/httpRequest'
export function fetchList(params) {
  return http({
    url:http.adornUrl('/productAttribute/category/list'),
    method:'get',
    params:params
  })
}

export function createProductAttrCate(params) {
  return http({
    url:http.adornUrl('/productAttribute/category/create'),
    method:'post',
    params:params
  })
}

export function deleteProductAttrCate(id) {
  return http({
    url:http.adornUrl('/productAttribute/category/delete/'+id),
    method:'get'
  })
}

export function updateProductAttrCate(id,params) {
  return http({
    url:http.adornUrl('/productAttribute/category/update/'+id),
    method:'post',
    params:params
  })
}
export function fetchListWithAttr() {
  return http({
    url:http.adornUrl('/productAttribute/category/list/withAttr'),
    method:'get'
  })
}
