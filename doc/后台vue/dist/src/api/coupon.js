import http from '@/utils/httpRequest'
export function fetchList(params) {
  return http({
    url:http.adornUrl('/coupon/list'),
    method:'get',
    params:params
  })
}

export function createCoupon(data) {
  return http({
    url:http.adornUrl('/coupon/create'),
    method:'post',
    data:data
  })
}

export function getCoupon(id) {
  return http({
    url:http.adornUrl('/coupon/'+id),
    method:'get',
  })
}

export function updateCoupon(id,data) {
  return http({
    url:http.adornUrl('/coupon/update/'+id),
    method:'post',
    data:data
  })
}

export function deleteCoupon(id) {
  return http({
    url:http.adornUrl('/coupon/delete/'+id),
    method:'post',
  })
}
