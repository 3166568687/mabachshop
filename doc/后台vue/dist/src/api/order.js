import http from '@/utils/httpRequest'
export function fetchList(params) {
  return http({
    url:http.adornUrl('/order/list'),
    method:'get',
    params:params
  })
}

export function closeOrder(params) {
  return http({
    url:http.adornUrl('/order/update/close'),
    method:'post',
    params:params
  })
}

export function deleteOrder(params) {
  return http({
    url:http.adornUrl('/order/delete'),
    method:'post',
    params:params
  })
}

export function deliveryOrder(data) {
  return http({
    url:http.adornUrl('/order/update/delivery'),
    method:'post',
    data:data
  });
}

export function getOrderDetail(id) {
  return http({
    url:http.adornUrl('/order/'+id),
    method:'get'
  });
}

export function updateReceiverInfo(data) {
  return http({
    url:http.adornUrl('/order/update/receiverInfo'),
    method:'post',
    data:data
  });
}

export function updateMoneyInfo(data) {
  return http({
    url:http.adornUrl('/order/update/moneyInfo'),
    method:'post',
    data:data
  });
}

export function updateOrderNote(params) {
  return http({
    url:http.adornUrl('/order/update/note'),
    method:'post',
    params:params
  })
}
