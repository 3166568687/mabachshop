import http from '@/utils/httpRequest'
export function fetchList(params) {
  return http({
    url:http.adornUrl('/memberLevel/list'),
    method:'get',
    params:params
  })
}
