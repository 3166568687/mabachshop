import http from '@/utils/httpRequest'
export function getOrderSetting(id) {
  return http({
    url:http.adornUrl('/orderSetting/'+id),
    method:'get',
  })
}

export function updateOrderSetting(id,data) {
  return http({
    url:http.adornUrl('/orderSetting/update/'+id),
    method:'post',
    data:data
  })
}
