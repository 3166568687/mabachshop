/*
SQLyog Ultimate v8.32 
MySQL - 5.7.21 : Database - system
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`system` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `system`;

/*Table structure for table `sys_captcha` */

DROP TABLE IF EXISTS `sys_captcha`;

CREATE TABLE `sys_captcha` (
  `uuid` char(36) NOT NULL COMMENT 'uuid',
  `code` varchar(6) NOT NULL COMMENT '验证码',
  `expire_time` datetime DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统验证码';

/*Data for the table `sys_captcha` */

insert  into `sys_captcha`(`uuid`,`code`,`expire_time`) values ('00bc4807-9c1e-4a87-87aa-25b124536871','abwg2','2020-01-07 03:21:02'),('024617e1-06f8-4c64-80c2-1d44f2ab3e5d','ngpd7','2020-01-11 18:17:08'),('047123dd-6ab5-4ce9-825b-3acb3d714a12','8cy5w','2020-01-11 14:00:02'),('06b13a73-e3ef-4e30-8e5a-61b12370f2ed','e3n75','2020-01-11 18:18:09'),('06fa34f8-3119-48fa-8a69-c95a7487b9bd','n5e5y','2020-01-11 19:13:46'),('080817e4-ef1b-4628-80b0-dba42712c7ab','bcdda','2020-01-11 18:07:44'),('0a0ee959-75ba-4327-8596-040bacc257dc','wn8b8','2020-01-11 13:23:17'),('0c2ae0db-7b61-4b29-8eef-39d01152c678','fa2fw','2020-01-11 19:13:59'),('0c7ed5cb-ab60-4a7d-88e7-dce7ce089661','25n6p','2020-01-11 14:42:05'),('0ec3792e-bbd9-421e-8454-496e7a60a37d','47aa6','2020-01-11 14:24:59'),('1088eb24-8ead-43dc-8748-9fe859970128','2w6mf','2020-01-11 18:16:58'),('13c56488-b7c2-4cd9-80eb-57e20e18e20d','ngfap','2020-01-11 18:08:16'),('177ac10c-53e2-45b0-8eaf-0e3c43ad20e9','7xn73','2020-01-11 19:14:05'),('18063b66-7620-424f-87f3-88e967604230','mpd32','2020-01-11 18:07:33'),('1819ec46-119f-4032-8818-221fd1b39f40','285f4','2020-01-11 11:52:14'),('1916a33b-bce2-4447-8c98-1614dcaf1375','cbdgy','2020-01-11 18:16:42'),('193c1d3a-c051-4b76-8d5a-611b9338f90c','37cm4','2020-01-11 14:24:54'),('1af7eb67-d346-42db-8e02-e7959eb30331','254x5','2020-01-11 13:20:34'),('1d4543b5-0d6c-4fe6-8090-901f3f01ac05','3fnec','2020-01-11 19:14:53'),('1e9e5fd5-7a05-4904-81ac-0ab43e503f73','3n7n5','2020-01-11 18:00:19'),('1eb6176c-ec95-4478-8309-1c9082848013','efnee','2020-01-11 18:00:19'),('1f27a9d3-6a99-4b33-8036-10628be11d7e','ya5m2','2020-01-11 18:07:12'),('24f4a643-37b5-4455-83eb-c45219dc5681','478w6','2020-01-11 17:59:57'),('27c48725-e16b-4b46-8614-7a4bc9b17d21','5mpwf','2020-01-11 13:20:56'),('2831ea2d-83dd-4830-8870-c5ebf1d79f42','cdabd','2020-01-11 12:14:52'),('287e9e9d-a2a7-46dd-8330-9eccecc99f21','pbbwg','2020-01-11 02:17:27'),('2ac03a8c-4068-4c42-8817-1179d84b04bc','gfep6','2020-01-11 18:16:47'),('2f8a76ec-b91f-450f-83f2-1f51c94882ab','dc4ew','2020-01-11 18:08:16'),('3011b5fe-baec-4c07-874b-146d66a7036d','amfpn','2020-01-11 18:07:53'),('3012705e-b276-4ee0-8541-e650827ca41c','6744n','2020-01-11 14:00:25'),('36903a63-6258-4325-8034-7130be68516c','6f5ym','2020-01-11 17:59:49'),('3a339b6f-2422-488d-8d16-1b5503be4fba','mn3n2','2020-01-11 12:59:44'),('3c80c8b9-54bc-4d30-87b0-ab0c4d706a8b','86cn3','2020-01-11 18:00:13'),('442859c1-9775-4555-8ee1-29b56047e1b6','ceggf','2020-01-11 18:07:33'),('457e56c6-5c01-4b03-862d-73a2775bc575','d6fbp','2020-01-11 19:14:17'),('4670c3fb-c4c9-4ebc-8887-94014e8aff28','fbg25','2020-01-11 18:18:42'),('47ba9371-62e1-4742-838c-e780db911650','am3mn','2020-01-11 18:18:09'),('48d1eb96-b381-4db4-862d-9ad21df4ee40','wgwce','2020-01-11 17:59:57'),('49e6222b-ebf1-4a14-8b91-1f55c0d79a4e','e5fbp','2020-01-11 18:07:33'),('4d437f5a-748e-4002-88f1-1af2d98d4df7','ndny5','2020-01-11 14:00:07'),('4dbd3bd3-a570-4f9c-850f-b3391f7ef817','g5w8f','2020-01-11 15:24:14'),('4ea12890-8842-421d-80c7-dcdb6bd2f6f2','f84e7','2020-01-11 19:13:59'),('4f181750-6362-4a46-8b29-cb994acacc23','x66e6','2020-01-11 19:14:26'),('4ff3eb72-993a-44d8-8489-c56b8e7c6515','6dy3m','2020-01-11 14:07:23'),('50854cb4-b104-4687-85ae-7feb62641510','nenbb','2020-01-11 19:13:24'),('508e98c6-d25b-422b-89eb-b665511fefb2','be4wm','2020-01-11 15:25:56'),('523f7e53-5efa-4c4a-8925-a64f7c3fa1a1','4nnmx','2020-01-11 18:07:45'),('53a2543d-2178-4f9e-8f96-f20b68c423b2','pd3w4','2020-01-11 13:00:10'),('55475df9-1dad-40ef-848b-573a71ad74c3','ne377','2020-01-11 18:18:04'),('55620c22-6581-42ee-8589-def64db80687','fpbdg','2020-01-11 14:41:58'),('5643f8e0-faf5-4729-84ac-b89494923066','nd8ex','2020-01-11 18:08:16'),('568baf92-ec51-4cb0-8af1-c4e6e4d19960','48wae','2020-01-11 13:23:20'),('5add96bf-6b0c-48ed-80a1-6205b645b6f1','nepbc','2020-01-11 14:25:05'),('5bcd7fde-0cfd-4dcb-8bcc-e4eccd0e1da5','ynbd6','2020-01-11 19:13:37'),('5e074864-90c7-491b-8673-7377d4355b35','p33p8','2020-01-11 12:15:02'),('612aa603-954b-4c65-8cb0-a0802f22a948','6755n','2020-01-11 17:18:25'),('6193931a-98dc-4157-876e-16e077e047ba','w48ac','2020-01-11 12:59:49'),('61e89363-eed2-4472-831b-e472935f4cd2','xaa5a','2020-01-11 19:13:24'),('684ea7ff-433d-46bc-82b8-0ebeaf9abf11','ng7ym','2020-01-11 18:18:04'),('68cffbc5-dadf-4b6e-88de-be2ac5360dec','n3pep','2020-01-11 18:18:42'),('69a1692a-df64-4d4c-86f8-e5e78ed9c66e','m5w6w','2020-01-11 13:55:49'),('69b13548-bdba-4741-8971-a6a3dee909e8','ng4na','2020-01-11 19:14:05'),('6a2b8e20-59d3-4813-8829-e7dd7fb78239','4pmn6','2020-01-11 19:13:37'),('6a5d8846-eb7d-461d-8d45-fea57cee7de8','5np2g','2020-01-11 15:24:21'),('6c770ef1-6de1-4531-8a34-e70f025ba993','c6we2','2020-01-11 19:13:53'),('702a0768-0a01-4998-8c28-7166adecbd4d','neen2','2020-01-11 18:18:32'),('725b1ea5-c76d-4346-84e7-fcde5359d2b8','beene','2020-01-11 19:14:21'),('72d0cc3f-eb02-42b7-815f-a16fa17900fe','f2562','2020-01-11 14:27:59'),('74b4d1da-e130-48aa-8477-4f7c066d0a35','436g6','2020-01-11 12:14:48'),('75baf278-5526-4cb4-8bc7-b97e4d069a97','xae4n','2020-01-11 19:13:46'),('77884059-8e8f-4cd1-8258-01b512bf309e','c4exb','2020-01-11 19:14:21'),('77b162d9-6fc8-4311-8f73-f81ea991e37f','nnygc','2020-01-11 18:00:10'),('78e3ef6a-0fc1-4dc9-88cc-a74a3cdba9cd','py87n','2020-01-11 19:13:52'),('7c82f47a-2d91-4f89-88e2-40c4a36926ac','4cmg5','2020-01-11 18:07:12'),('7f709adf-24ed-482e-8a7f-f1ab2b78818d','dpfx4','2020-01-11 14:42:17'),('80e9a4d4-8877-4fe6-8b0c-bd6c86492505','pfep7','2020-01-11 19:14:21'),('81bc4ac9-7d12-4c48-8c91-eb9db3a48bb6','75da2','2020-01-11 14:28:08'),('83564365-02e9-41b7-8cc7-53f53ffab755','xy4ef','2020-01-11 12:12:55'),('873e10a8-9df0-4f90-8cf1-9d6d2a2b573c','2x4px','2020-01-11 15:25:31'),('88db8724-7ca4-4c1f-82dc-e15d7e466da0','d3anc','2020-01-11 19:14:15'),('88de37c0-29ca-47b6-888e-7858456d636f','68bne','2020-01-11 18:06:53'),('89aaa5bd-38d7-4e11-8f6d-10f7de8e8d45','w5pf6','2020-01-11 18:07:36'),('8b0558c5-8fd9-4dc1-89e3-0cc804bbf9be','naawn','2020-01-11 19:13:52'),('8d4b238b-ad57-479c-890d-23beb460e74f','bpw4y','2020-01-11 19:13:37'),('91009440-a1fb-4d06-8a7c-7261930d39dc','a36xp','2020-01-11 19:14:54'),('912d8bbd-8454-4093-8c8d-cd3c73f72522','583mp','2020-01-11 13:52:37'),('916734e9-f59c-4e9f-8239-d3f4ebcf1d00','dxnfw','2020-01-11 13:01:08'),('955e3b96-fccb-4e7e-86c9-c7d925144608','anfgn','2020-01-11 19:13:24'),('95e2077e-d638-424b-871b-a992ad423ac8','ndx86','2020-01-11 18:07:12'),('97fa7fb9-2d96-41e2-8551-6907413ee33b','22b2e','2020-01-11 14:28:17'),('99d39cb4-f3d6-4e6f-8432-d5971c3bc0a2','wy6gw','2020-01-11 18:06:53'),('9a1f14a5-5e0e-4007-83d5-0fe8aa0b0575','c4x4w','2020-01-11 19:14:05'),('9d740b06-f190-4796-8570-8e046debdaa0','p7f2y','2020-01-11 18:18:32'),('9ea65dd0-6abd-49f5-83b7-5b68a7fb7ffc','64mx6','2020-01-11 18:18:04'),('9fc28f6a-6886-4fb5-848c-4aa94fca0124','ep8n6','2020-01-11 19:13:58'),('a1d71ba4-d911-4551-8490-76cf2a95db50','px8cb','2020-01-11 02:13:38'),('a354a188-1e0c-45f4-8291-486cc3c75bd6','efn5p','2020-01-11 12:59:57'),('a361daed-579e-4e7a-89e0-a45d5bc0c4ad','xbxnc','2020-01-11 14:41:54'),('a694ce3a-046c-4f4f-8e3f-c63b23a13c6c','4c4yn','2020-01-11 18:07:02'),('a7c37031-dde7-4e97-878d-19ef089e09d1','g8fw3','2020-01-07 00:24:38'),('a7e08452-5245-488f-89cd-50a91c216283','8y4w2','2020-01-11 18:00:02'),('a99c8789-7b0e-4462-8296-1c2bb84e802a','y4gaa','2020-01-11 18:06:42'),('aa007ed7-fbb6-4673-80c2-9df878dbfad0','mbcff','2020-01-11 13:00:58'),('ac104e9f-a7c4-4065-8b29-ad8aadf5f829','7ey3w','2020-01-11 13:21:05'),('ae1fe255-35ea-4110-89f5-142ad2f9dac6','7wn2c','2020-01-11 18:07:02'),('afcd860b-49de-4348-85fb-dce28a364751','d82ye','2020-01-11 19:14:16'),('b148e321-465a-4bce-88dd-99c2bcac0b9d','n6cfa','2020-01-07 14:07:13'),('b462a9df-6358-45b6-8613-304bb4ae44d3','875c7','2020-01-11 14:00:15'),('b4a055ef-f105-483c-8585-3418b87bae50','agmw3','2020-01-11 18:16:58'),('b5f204b3-54b0-4526-869e-2d0156558187','5n72d','2020-01-11 18:18:22'),('b62eda87-3421-4c25-8ad1-395982957e4a','cffb4','2020-01-11 13:23:34'),('b9983e35-24c1-4659-8290-60260e4e27f3','268ad','2020-01-11 18:00:10'),('bd00496c-b62a-49b8-88d5-2c1e78877faf','dmnaa','2020-01-11 18:00:02'),('bd2d83e3-c2b8-438f-86e0-482037c92331','gda7f','2020-01-11 13:00:17'),('c1245df6-2e9b-4761-8997-14d4fa947069','n4yp3','2020-01-11 18:17:09'),('c144972d-c4d0-4148-8f4c-3d72e6034b09','w6wna','2020-01-11 13:23:24'),('c15a27f8-e646-4e7b-888a-3688ed70565c','5wccm','2020-01-11 18:16:47'),('c3769842-bccd-428b-8b7b-0f219c3485c6','7yx38','2020-01-11 18:06:54'),('c42ed52c-e7e2-4b30-8d7b-7a63273fb63d','dg58x','2020-01-11 14:27:47'),('c53719b2-3903-43b8-8769-2f7653068865','3a64p','2020-01-11 18:07:53'),('c7223ea9-489e-46bd-82cc-4602db81b589','f2cng','2020-01-11 17:59:49'),('c750477c-d945-4804-8f36-13df2e2661f1','dpg5c','2020-01-11 17:19:06'),('cadeb790-15dd-423e-885c-2aa51cfa4e69','gxeb8','2020-01-11 12:14:59'),('d0f30e58-5c9c-4c93-8289-f6c36699e1a8','cg66c','2020-01-11 18:18:32'),('d1dfd3f3-eec8-4c56-8ad7-c6572bae4cd2','bnnda','2020-01-11 18:07:53'),('d69c94e2-13e7-4053-8e1a-8e7d205c9d6b','g2577','2020-01-11 13:20:42'),('d6f9b318-3000-425f-8c1b-a7c04ae6ced9','ym3px','2020-01-11 17:59:57'),('db35680f-80f9-4fbf-8602-9e2cd82b7e38','aw5mn','2020-01-11 14:24:44'),('deae4c32-c534-4907-8345-8db87baa753a','a4c3y','2020-01-11 15:24:33'),('e0374ef5-6337-4f05-8028-16633cf449be','ybadp','2020-01-11 18:18:22'),('e4ec92b5-3f88-4fdd-8312-f56672c5a3c6','yawf2','2020-01-11 18:06:42'),('e58b1676-cede-4f8f-873a-a20f6e58b384','epnd4','2020-01-11 18:00:19'),('e5d849bb-6ea0-43b0-8fda-e78584a00d02','baap5','2020-01-11 18:07:45'),('e603af39-539e-4f4a-8dc9-bed49ac57c5a','px2yn','2020-01-11 17:59:49'),('e6220e3b-4799-4618-8864-2e3c50d8a16c','nc4f7','2020-01-11 18:00:02'),('e6bb076b-ce84-4c4e-8d34-11fb5d852fa0','2d8bb','2020-01-11 19:13:46'),('e7b370b3-d932-4b79-81ed-8840305f91b2','8xyaw','2020-01-11 18:07:01'),('e8f62c4f-26f6-475e-8b70-3f78d5046b33','x2y6d','2020-01-11 15:25:38'),('eb3a4f8a-30ac-4bce-828e-fa1bb4406870','54c4w','2020-01-11 18:18:09'),('ebe64a2f-fae4-4079-8b22-9e0d581ced73','7mgwg','2020-01-11 18:06:43'),('ec66bad7-64d7-4c78-8f8f-b9aa9a41817b','7xbn5','2020-01-11 18:17:09'),('f0a05d30-c9f7-4faf-8de1-8c240fad2252','mf6c5','2020-01-11 13:24:15'),('f21b0744-a65f-4afd-8c27-6faae2120995','ycy5m','2020-01-11 15:25:46'),('f2cf38c5-747d-4a00-8b7f-dcac010add19','8aege','2020-01-11 18:00:10'),('f3f370d1-e9f1-49ea-83c9-a15a0d83f94e','m68nb','2020-01-10 23:38:16'),('f6b97162-f4a0-43c2-819d-cd806cec8306','fbm6a','2020-01-11 18:00:13'),('fb129e88-5b5c-48b8-854e-241334d70f8e','725ca','2020-01-11 15:24:49'),('fc123533-a14a-4efe-890f-2163b67deb23','c2exp','2020-01-11 18:16:45'),('fcaaafbf-2bc3-471f-83e4-40ffbe903310','pmc3e','2020-01-11 18:18:22'),('fd40a8ab-84bc-4651-8747-648d1b735cc2','4em42','2020-01-11 13:01:02'),('fd900f5d-4292-4107-8fe5-b9d14cbbd456','pm6d5','2020-01-11 18:16:43'),('fe79db39-b876-463c-8789-e276bd473bf4','camgx','2020-01-11 18:18:42'),('ff3111c7-6143-4d88-87b4-93cb4abca5e4','3ef6d','2020-01-11 11:39:20'),('ffeb93c3-cf1a-4128-8c29-6ff8d39a5d61','ne7yy','2020-01-11 18:16:58');

/*Table structure for table `sys_log` */

DROP TABLE IF EXISTS `sys_log`;

CREATE TABLE `sys_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) DEFAULT NULL COMMENT '用户操作',
  `method` varchar(200) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `time` bigint(20) NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4 COMMENT='系统日志';

/*Data for the table `sys_log` */

insert  into `sys_log`(`id`,`username`,`operation`,`method`,`params`,`time`,`ip`,`create_date`) values (1,'admin','修改菜单','io.renren.modules.sys.controller.SysMenuController.update()','[{\"menuId\":5,\"parentId\":1,\"name\":\"SQL监控\",\"url\":\"http://localhost:8080/druid/sql.html\",\"type\":1,\"icon\":\"sql\",\"orderNum\":4}]',9,'127.0.0.1','2019-12-18 14:17:11'),(2,'admin','修改菜单','io.renren.modules.sys.controller.SysMenuController.update()','[{\"menuId\":5,\"parentId\":1,\"name\":\"SQL监控\",\"url\":\"http://localhost:8081/druid/sql.html\",\"type\":1,\"icon\":\"sql\",\"orderNum\":4}]',7,'127.0.0.1','2019-12-18 14:17:48'),(3,'admin','暂停定时任务','io.renren.modules.job.controller.ScheduleJobController.pause()','[[1]]',74,'127.0.0.1','2019-12-18 14:19:46'),(4,'admin','删除定时任务','io.renren.modules.job.controller.ScheduleJobController.delete()','[[1]]',40,'127.0.0.1','2019-12-18 14:19:51'),(5,'admin','修改菜单','SysMenuController.update()','[{\"menuId\":5,\"parentId\":1,\"name\":\"SQL监控\",\"url\":\"http://localhost:8082/druid/sql.html\",\"type\":1,\"icon\":\"sql\",\"orderNum\":4}]',8,'127.0.0.1','2019-12-18 14:57:04'),(6,'admin','保存菜单','SysMenuController.save()','[{\"menuId\":31,\"parentId\":0,\"name\":\"商品管理\",\"url\":\"\",\"perms\":\"\",\"type\":0,\"icon\":\"\",\"orderNum\":0}]',15,'127.0.0.1','2019-12-18 22:50:00'),(7,'admin','保存菜单','SysMenuController.save()','[{\"menuId\":32,\"parentId\":31,\"name\":\"相册管理\",\"url\":\"goods/album\",\"perms\":\"\",\"type\":1,\"icon\":\"\",\"orderNum\":0}]',7,'127.0.0.1','2019-12-18 22:50:51'),(8,'admin','保存角色','SysRoleController.save()','[{\"roleId\":1,\"roleName\":\"v\",\"remark\":\"\",\"createUserId\":1,\"menuIdList\":[1,2,15,16,17,18,3,19,20,21,22,4,23,24,25,26,5,6,7,8,9,10,11,12,13,14,27,29,30,31,32,-666666],\"createTime\":\"Dec 18, 2019 10:53:37 PM\"}]',208,'127.0.0.1','2019-12-18 22:53:38'),(9,'admin','修改用户','SysUserController.update()','[{\"userId\":1,\"username\":\"admin\",\"salt\":\"YzcmCZNvbXocrsz9dm8e\",\"email\":\"root@renren.io\",\"mobile\":\"13612345678\",\"status\":1,\"roleIdList\":[1],\"createUserId\":1}]',86,'127.0.0.1','2019-12-18 22:53:55'),(10,'admin','修改菜单','io.renren.modules.sys.controller.SysMenuController.update()','[{\"menuId\":32,\"parentId\":31,\"name\":\"相册管理\",\"url\":\"pms/brand/index\",\"perms\":\"\",\"type\":1,\"icon\":\"\",\"orderNum\":0}]',10,'127.0.0.1','2019-12-28 23:08:07'),(11,'admin','修改菜单','io.renren.modules.sys.controller.SysMenuController.update()','[{\"menuId\":32,\"parentId\":31,\"name\":\"相册管理\",\"url\":\"pms/product/index\",\"perms\":\"\",\"type\":1,\"icon\":\"\",\"orderNum\":0}]',8,'127.0.0.1','2019-12-28 23:13:37'),(12,'admin','修改菜单','io.renren.modules.sys.controller.SysMenuController.update()','[{\"menuId\":32,\"parentId\":31,\"name\":\"商品信息\",\"url\":\"pms/product/index\",\"perms\":\"pms:product:read,pms:productCategory:read,pms:brand:read\",\"type\":1,\"icon\":\"\",\"orderNum\":0}]',16,'127.0.0.1','2019-12-29 17:19:48'),(13,'admin','修改菜单','io.renren.modules.sys.controller.SysMenuController.update()','[{\"menuId\":32,\"parentId\":31,\"name\":\"商品信息\",\"url\":\"pms/product\",\"perms\":\"pms:product:read,pms:productCategory:read,pms:brand:read\",\"type\":1,\"icon\":\"\",\"orderNum\":0}]',8,'127.0.0.1','2019-12-29 17:20:39'),(14,'admin','修改菜单','io.renren.modules.sys.controller.SysMenuController.update()','[{\"menuId\":32,\"parentId\":31,\"name\":\"商品信息\",\"url\":\"pms/product/index\",\"perms\":\"pms:product:read,pms:productCategory:read,pms:brand:read\",\"type\":1,\"icon\":\"\",\"orderNum\":0}]',11,'127.0.0.1','2019-12-29 17:22:15'),(15,'admin','保存菜单','io.renren.modules.sys.controller.SysMenuController.save()','[{\"menuId\":33,\"parentId\":31,\"name\":\"添加商品\",\"url\":\"pms\",\"perms\":\"\",\"type\":1,\"icon\":\"\",\"orderNum\":0}]',8,'127.0.0.1','2019-12-29 17:22:54'),(16,'admin','修改菜单','io.renren.modules.sys.controller.SysMenuController.update()','[{\"menuId\":33,\"parentId\":31,\"name\":\"添加商品\",\"url\":\"pms/product/index\",\"perms\":\"\",\"type\":1,\"icon\":\"\",\"orderNum\":0}]',7,'127.0.0.1','2019-12-29 17:23:08'),(17,'admin','修改菜单','io.renren.modules.sys.controller.SysMenuController.update()','[{\"menuId\":33,\"parentId\":31,\"name\":\"添加商品\",\"url\":\"pms/product/components/\",\"perms\":\"\",\"type\":1,\"icon\":\"\",\"orderNum\":0}]',4,'127.0.0.1','2019-12-29 17:23:30'),(18,'admin','修改菜单','io.renren.modules.sys.controller.SysMenuController.update()','[{\"menuId\":33,\"parentId\":31,\"name\":\"添加商品\",\"url\":\"pms/product/components/ProductDetail\",\"perms\":\"\",\"type\":1,\"icon\":\"\",\"orderNum\":0}]',8,'127.0.0.1','2019-12-29 17:23:44'),(19,'admin','修改菜单','io.renren.modules.sys.controller.SysMenuController.update()','[{\"menuId\":33,\"parentId\":31,\"name\":\"添加商品\",\"url\":\"pms/product/components/ProductDetail.vue\",\"perms\":\"\",\"type\":1,\"icon\":\"\",\"orderNum\":0}]',8,'127.0.0.1','2019-12-29 17:24:58'),(20,'admin','修改菜单','io.renren.modules.sys.controller.SysMenuController.update()','[{\"menuId\":33,\"parentId\":31,\"name\":\"添加商品\",\"url\":\"pms/product/add\",\"perms\":\"\",\"type\":1,\"icon\":\"\",\"orderNum\":0}]',8,'127.0.0.1','2019-12-29 17:25:26'),(21,'admin','修改菜单','io.renren.modules.sys.controller.SysMenuController.update()','[{\"menuId\":32,\"parentId\":31,\"name\":\"商品信息\",\"url\":\"pms/brand/index\",\"perms\":\"pms:product:read,pms:productCategory:read,pms:brand:read\",\"type\":1,\"icon\":\"\",\"orderNum\":0}]',11,'192.168.65.1','2020-01-07 12:56:35'),(22,'admin','修改菜单','io.renren.modules.sys.controller.SysMenuController.update()','[{\"menuId\":32,\"parentId\":31,\"name\":\"商品信息\",\"url\":\"pms/brand/index\",\"perms\":\"pms:product:read,pms:productCategory:read,pms:brand:read\",\"type\":1,\"icon\":\"\",\"orderNum\":0}]',10,'192.168.65.1','2020-01-07 12:56:51'),(23,'admin','修改菜单','io.renren.modules.sys.controller.SysMenuController.update()','[{\"menuId\":32,\"parentId\":31,\"name\":\"品牌管理\",\"url\":\"pms/brand/index\",\"perms\":\"\",\"type\":1,\"icon\":\"\",\"orderNum\":0}]',12,'192.168.65.1','2020-01-07 17:24:50'),(24,'admin','保存菜单','io.renren.modules.sys.controller.SysMenuController.save()','[{\"menuId\":34,\"parentId\":0,\"name\":\"订单管理\",\"url\":\"\",\"perms\":\"\",\"type\":0,\"icon\":\"zonghe\",\"orderNum\":4}]',9,'192.168.65.1','2020-01-10 23:37:41'),(25,'admin','修改菜单','SysMenuController.update()','[{\"menuId\":33,\"parentId\":31,\"name\":\"添加商品\",\"url\":\"pms/product/index\",\"perms\":\"\",\"type\":1,\"icon\":\"\",\"orderNum\":0}]',34,'192.168.65.1','2020-01-11 11:29:37'),(26,'admin','修改菜单','SysMenuController.update()','[{\"menuId\":33,\"parentId\":31,\"name\":\"商品管理\",\"url\":\"pms/product/index\",\"perms\":\"\",\"type\":1,\"icon\":\"\",\"orderNum\":0}]',8,'192.168.65.1','2020-01-11 11:29:59'),(27,'admin','保存菜单','SysMenuController.save()','[{\"menuId\":35,\"parentId\":31,\"name\":\"商品回收站\",\"url\":\"#\",\"perms\":\"\",\"type\":1,\"icon\":\"shanchu\",\"orderNum\":0}]',17,'192.168.65.1','2020-01-11 12:20:53'),(28,'admin','修改菜单','SysMenuController.update()','[{\"menuId\":31,\"parentId\":0,\"name\":\"商品管理\",\"url\":\"\",\"perms\":\"\",\"type\":0,\"icon\":\"tixing\",\"orderNum\":0}]',10,'192.168.65.1','2020-01-11 12:21:24'),(29,'admin','保存菜单','SysMenuController.save()','[{\"menuId\":36,\"parentId\":31,\"name\":\"商品分类\",\"url\":\"/pms/productCate/index\",\"perms\":\"\",\"type\":1,\"icon\":\"zhedie\",\"orderNum\":4}]',51,'192.168.65.1','2020-01-11 12:22:57'),(30,'admin','保存菜单','SysMenuController.save()','[{\"menuId\":37,\"parentId\":31,\"name\":\"商品类型\",\"url\":\"/pms/productAttr/index\",\"perms\":\"\",\"type\":1,\"icon\":\"log\",\"orderNum\":6}]',11,'192.168.65.1','2020-01-11 13:13:59'),(31,'admin','修改菜单','SysMenuController.update()','[{\"menuId\":32,\"parentId\":31,\"name\":\"品牌管理\",\"url\":\"pms/brand/index\",\"perms\":\"\",\"type\":1,\"icon\":\"\",\"orderNum\":7}]',14,'192.168.65.1','2020-01-11 14:00:11'),(32,'admin','修改菜单','SysMenuController.update()','[{\"menuId\":32,\"parentId\":31,\"name\":\"品牌管理\",\"url\":\"pms/brand/index\",\"perms\":\"\",\"type\":1,\"icon\":\"tubiao\",\"orderNum\":7}]',8,'192.168.65.1','2020-01-11 14:01:06'),(33,'admin','修改菜单','SysMenuController.update()','[{\"menuId\":33,\"parentId\":31,\"name\":\"商品管理\",\"url\":\"pms/product/index\",\"perms\":\"\",\"type\":1,\"icon\":\"menu\",\"orderNum\":0}]',9,'192.168.65.1','2020-01-11 14:01:32'),(34,'admin','保存菜单','SysMenuController.save()','[{\"menuId\":38,\"parentId\":34,\"name\":\"订单列表\",\"url\":\"/oms/order/index\",\"perms\":\"\",\"type\":1,\"icon\":\"sql\",\"orderNum\":0}]',12,'192.168.65.1','2020-01-11 14:07:42'),(35,'admin','保存菜单','SysMenuController.save()','[{\"menuId\":39,\"parentId\":34,\"name\":\"订单设置\",\"url\":\"/oms/order/setting\",\"perms\":\"\",\"type\":1,\"icon\":\"shezhi\",\"orderNum\":0}]',13,'192.168.65.1','2020-01-11 14:31:29'),(36,'admin','保存菜单','SysMenuController.save()','[{\"menuId\":40,\"parentId\":34,\"name\":\"退款申请\",\"url\":\"/oms/apply/index\",\"perms\":\"\",\"type\":1,\"icon\":\"role\",\"orderNum\":2}]',8,'192.168.65.1','2020-01-11 14:33:48'),(37,'admin','保存菜单','SysMenuController.save()','[{\"menuId\":41,\"parentId\":34,\"name\":\"退款原因设置\",\"url\":\"/oms/apply/reason\",\"perms\":\"\",\"type\":1,\"icon\":\"bianji\",\"orderNum\":4}]',10,'192.168.65.1','2020-01-11 14:39:47'),(38,'admin','保存菜单','SysMenuController.save()','[{\"menuId\":42,\"parentId\":34,\"name\":\"退款原因详情\",\"url\":\"/oms/apply/applyDetail\",\"perms\":\"\",\"type\":1,\"icon\":\"editor\",\"orderNum\":5}]',10,'192.168.65.1','2020-01-11 14:43:34'),(39,'admin','删除菜单','SysMenuController.delete()','[42]',25,'192.168.65.1','2020-01-11 14:44:29'),(40,'admin','修改菜单','SysMenuController.update()','[{\"menuId\":34,\"parentId\":0,\"name\":\"订单管理\",\"url\":\"\",\"perms\":\"\",\"type\":0,\"icon\":\"zonghe\",\"orderNum\":2}]',10,'192.168.65.1','2020-01-11 14:45:23'),(41,'admin','修改菜单','SysMenuController.update()','[{\"menuId\":31,\"parentId\":0,\"name\":\"商品管理\",\"url\":\"\",\"perms\":\"\",\"type\":0,\"icon\":\"tixing\",\"orderNum\":1}]',4,'192.168.65.1','2020-01-11 14:45:28'),(42,'admin','修改菜单','SysMenuController.update()','[{\"menuId\":1,\"parentId\":0,\"name\":\"系统管理\",\"type\":0,\"icon\":\"system\",\"orderNum\":5}]',5,'192.168.65.1','2020-01-11 14:45:33'),(43,'admin','保存菜单','SysMenuController.save()','[{\"menuId\":43,\"parentId\":0,\"name\":\"权限管理\",\"url\":\"#\",\"perms\":\"\",\"type\":1,\"icon\":\"admin\",\"orderNum\":3}]',4,'192.168.65.1','2020-01-11 14:46:05'),(44,'admin','删除菜单','SysMenuController.delete()','[43]',11,'192.168.65.1','2020-01-11 14:46:14'),(45,'admin','保存菜单','SysMenuController.save()','[{\"menuId\":44,\"parentId\":0,\"name\":\"权限管理\",\"url\":\"\",\"perms\":\"\",\"type\":0,\"icon\":\"admin\",\"orderNum\":3}]',6,'192.168.65.1','2020-01-11 14:46:31'),(46,'admin','修改菜单','SysMenuController.update()','[{\"menuId\":2,\"parentId\":44,\"name\":\"管理员列表\",\"url\":\"sys/user\",\"type\":1,\"icon\":\"admin\",\"orderNum\":1}]',8,'192.168.65.1','2020-01-11 14:46:47'),(47,'admin','修改菜单','SysMenuController.update()','[{\"menuId\":3,\"parentId\":44,\"name\":\"角色管理\",\"url\":\"sys/role\",\"type\":1,\"icon\":\"role\",\"orderNum\":2}]',6,'192.168.65.1','2020-01-11 14:46:58'),(48,'admin','修改菜单','SysMenuController.update()','[{\"menuId\":4,\"parentId\":44,\"name\":\"菜单管理\",\"url\":\"sys/menu\",\"type\":1,\"icon\":\"menu\",\"orderNum\":3}]',8,'192.168.65.1','2020-01-11 14:47:23'),(49,'admin','修改用户','SysUserController.update()','[{\"userId\":1,\"username\":\"admin\",\"password\":\"9ec9750e709431dad22365cabc5c625482e574c74adaebba7dd02f1129e4ce1d\",\"salt\":\"YzcmCZNvbXocrsz9dm8e\",\"email\":\"mabach@mabach.cn\",\"mobile\":\"13612345678\",\"status\":1,\"roleIdList\":[1],\"createUserId\":1}]',417,'192.168.65.1','2020-01-11 14:47:54'),(50,'admin','保存菜单','SysMenuController.save()','[{\"menuId\":45,\"parentId\":0,\"name\":\"推广运营\",\"url\":\"\",\"perms\":\"\",\"type\":0,\"icon\":\"oss\",\"orderNum\":4}]',14,'192.168.65.1','2020-01-11 15:03:54'),(51,'admin','保存菜单','SysMenuController.save()','[{\"menuId\":46,\"parentId\":45,\"name\":\"秒杀活动\",\"url\":\"/sms/flash/index\",\"perms\":\"\",\"type\":1,\"icon\":\"job\",\"orderNum\":0}]',9,'192.168.65.1','2020-01-11 15:04:48'),(52,'admin','修改菜单','SysMenuController.update()','[{\"menuId\":46,\"parentId\":45,\"name\":\"秒杀活动\",\"url\":\"/oms/apply/index\",\"perms\":\"\",\"type\":1,\"icon\":\"job\",\"orderNum\":0}]',17,'192.168.65.1','2020-01-11 15:07:16'),(53,'admin','修改菜单','SysMenuController.update()','[{\"menuId\":46,\"parentId\":45,\"name\":\"秒杀活动\",\"url\":\"/sms/flash/index\",\"perms\":\"\",\"type\":1,\"icon\":\"job\",\"orderNum\":0}]',7,'192.168.65.1','2020-01-11 15:08:44'),(54,'admin','保存菜单','SysMenuController.save()','[{\"menuId\":47,\"parentId\":45,\"name\":\"优惠券\",\"url\":\"/sms/coupon/index\",\"perms\":\"\",\"type\":1,\"icon\":\"daohang\",\"orderNum\":2}]',10,'192.168.65.1','2020-01-11 18:00:38'),(55,'admin','保存菜单','SysMenuController.save()','[{\"menuId\":48,\"parentId\":45,\"name\":\"品牌推荐\",\"url\":\"/sms/brand/index\",\"perms\":\"\",\"type\":1,\"icon\":\"geren\",\"orderNum\":3}]',12,'192.168.65.1','2020-01-11 18:14:13'),(56,'admin','保存菜单','SysMenuController.save()','[{\"menuId\":49,\"parentId\":45,\"name\":\"新品推荐\",\"url\":\"/sms/new/index\",\"perms\":\"\",\"type\":1,\"icon\":\"xiangqu\",\"orderNum\":4}]',17,'192.168.65.1','2020-01-11 18:31:55'),(57,'admin','保存菜单','SysMenuController.save()','[{\"menuId\":50,\"parentId\":45,\"name\":\"人气推荐\",\"url\":\"/sms/hot/index\",\"perms\":\"\",\"type\":1,\"icon\":\"shoucangfill\",\"orderNum\":5}]',6,'192.168.65.1','2020-01-11 18:39:56'),(58,'admin','保存菜单','SysMenuController.save()','[{\"menuId\":51,\"parentId\":45,\"name\":\"专题推荐\",\"url\":\"/sms/subject/index\",\"perms\":\"\",\"type\":1,\"icon\":\"mudedi\",\"orderNum\":7}]',12,'192.168.65.1','2020-01-11 18:52:59'),(59,'admin','保存菜单','SysMenuController.save()','[{\"menuId\":52,\"parentId\":45,\"name\":\"广告列表\",\"url\":\"/sms/advertise/index\",\"perms\":\"\",\"type\":1,\"icon\":\"editor\",\"orderNum\":8}]',7,'192.168.65.1','2020-01-11 18:58:30');

/*Table structure for table `sys_menu` */

DROP TABLE IF EXISTS `sys_menu`;

CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COMMENT='菜单管理';

/*Data for the table `sys_menu` */

insert  into `sys_menu`(`menu_id`,`parent_id`,`name`,`url`,`perms`,`type`,`icon`,`order_num`) values (1,0,'系统管理',NULL,NULL,0,'system',5),(2,44,'管理员列表','sys/user',NULL,1,'admin',1),(3,44,'角色管理','sys/role',NULL,1,'role',2),(4,44,'菜单管理','sys/menu',NULL,1,'menu',3),(5,1,'SQL监控','http://localhost:8082/druid/sql.html',NULL,1,'sql',4),(6,1,'定时任务','job/schedule',NULL,1,'job',5),(7,6,'查看',NULL,'sys:schedule:list,sys:schedule:info',2,NULL,0),(8,6,'新增',NULL,'sys:schedule:save',2,NULL,0),(9,6,'修改',NULL,'sys:schedule:update',2,NULL,0),(10,6,'删除',NULL,'sys:schedule:delete',2,NULL,0),(11,6,'暂停',NULL,'sys:schedule:pause',2,NULL,0),(12,6,'恢复',NULL,'sys:schedule:resume',2,NULL,0),(13,6,'立即执行',NULL,'sys:schedule:run',2,NULL,0),(14,6,'日志列表',NULL,'sys:schedule:log',2,NULL,0),(15,2,'查看',NULL,'sys:user:list,sys:user:info',2,NULL,0),(16,2,'新增',NULL,'sys:user:save,sys:role:select',2,NULL,0),(17,2,'修改',NULL,'sys:user:update,sys:role:select',2,NULL,0),(18,2,'删除',NULL,'sys:user:delete',2,NULL,0),(19,3,'查看',NULL,'sys:role:list,sys:role:info',2,NULL,0),(20,3,'新增',NULL,'sys:role:save,sys:menu:list',2,NULL,0),(21,3,'修改',NULL,'sys:role:update,sys:menu:list',2,NULL,0),(22,3,'删除',NULL,'sys:role:delete',2,NULL,0),(23,4,'查看',NULL,'sys:menu:list,sys:menu:info',2,NULL,0),(24,4,'新增',NULL,'sys:menu:save,sys:menu:select',2,NULL,0),(25,4,'修改',NULL,'sys:menu:update,sys:menu:select',2,NULL,0),(26,4,'删除',NULL,'sys:menu:delete',2,NULL,0),(27,1,'参数管理','sys/config','sys:config:list,sys:config:info,sys:config:save,sys:config:update,sys:config:delete',1,'config',6),(29,1,'系统日志','sys/log','sys:log:list',1,'log',7),(30,1,'文件上传','oss/oss','sys:oss:all',1,'oss',6),(31,0,'商品管理','','',0,'tixing',1),(32,31,'品牌管理','pms/brand/index','',1,'tubiao',7),(33,31,'商品管理','pms/product/index','',1,'menu',0),(34,0,'订单管理','','',0,'zonghe',2),(35,31,'商品回收站','#','',1,'shanchu',0),(36,31,'商品分类','/pms/productCate/index','',1,'zhedie',4),(37,31,'商品类型','/pms/productAttr/index','',1,'log',6),(38,34,'订单列表','/oms/order/index','',1,'sql',0),(39,34,'订单设置','/oms/order/setting','',1,'shezhi',0),(40,34,'退款申请','/oms/apply/index','',1,'role',2),(41,34,'退款原因设置','/oms/apply/reason','',1,'bianji',4),(44,0,'权限管理','','',0,'admin',3),(45,0,'推广运营','','',0,'oss',4),(46,45,'秒杀活动','/sms/flash/index','',1,'job',0),(47,45,'优惠券','/sms/coupon/index','',1,'daohang',2),(48,45,'品牌推荐','/sms/brand/index','',1,'geren',3),(49,45,'新品推荐','/sms/new/index','',1,'xiangqu',4),(50,45,'人气推荐','/sms/hot/index','',1,'shoucangfill',5),(51,45,'专题推荐','/sms/subject/index','',1,'mudedi',7),(52,45,'广告列表','/sms/advertise/index','',1,'editor',8);

/*Table structure for table `sys_oss` */

DROP TABLE IF EXISTS `sys_oss`;

CREATE TABLE `sys_oss` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(200) DEFAULT NULL COMMENT 'URL地址',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='文件上传';

/*Data for the table `sys_oss` */

/*Table structure for table `sys_role` */

DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='角色';

/*Data for the table `sys_role` */

insert  into `sys_role`(`role_id`,`role_name`,`remark`,`create_user_id`,`create_time`) values (1,'v','',1,'2019-12-18 22:53:38');

/*Table structure for table `sys_role_menu` */

DROP TABLE IF EXISTS `sys_role_menu`;

CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COMMENT='角色与菜单对应关系';

/*Data for the table `sys_role_menu` */

insert  into `sys_role_menu`(`id`,`role_id`,`menu_id`) values (1,1,1),(2,1,2),(3,1,15),(4,1,16),(5,1,17),(6,1,18),(7,1,3),(8,1,19),(9,1,20),(10,1,21),(11,1,22),(12,1,4),(13,1,23),(14,1,24),(15,1,25),(16,1,26),(17,1,5),(18,1,6),(19,1,7),(20,1,8),(21,1,9),(22,1,10),(23,1,11),(24,1,12),(25,1,13),(26,1,14),(27,1,27),(28,1,29),(29,1,30),(30,1,31),(31,1,32),(32,1,-666666);

/*Table structure for table `sys_user` */

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `salt` varchar(20) DEFAULT NULL COMMENT '盐',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) DEFAULT NULL COMMENT '手机号',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='系统用户';

/*Data for the table `sys_user` */

insert  into `sys_user`(`user_id`,`username`,`password`,`salt`,`email`,`mobile`,`status`,`create_user_id`,`create_time`) values (1,'admin','9ec9750e709431dad22365cabc5c625482e574c74adaebba7dd02f1129e4ce1d','YzcmCZNvbXocrsz9dm8e','mabach@mabach.cn','13612345678',1,1,'2019-11-11 11:11:11');

/*Table structure for table `sys_user_role` */

DROP TABLE IF EXISTS `sys_user_role`;

CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='用户与角色对应关系';

/*Data for the table `sys_user_role` */

insert  into `sys_user_role`(`id`,`user_id`,`role_id`) values (2,1,1);

/*Table structure for table `sys_user_token` */

DROP TABLE IF EXISTS `sys_user_token`;

CREATE TABLE `sys_user_token` (
  `user_id` bigint(20) NOT NULL,
  `token` varchar(100) NOT NULL COMMENT 'token',
  `expire_time` datetime DEFAULT NULL COMMENT '过期时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统用户Token';

/*Data for the table `sys_user_token` */

insert  into `sys_user_token`(`user_id`,`token`,`expire_time`,`update_time`) values (1,'f597eeccd0cd37264d86641fe0bd866f','2020-01-12 05:10:06','2020-01-11 17:10:06');

/*Table structure for table `tb_user` */

DROP TABLE IF EXISTS `tb_user`;

CREATE TABLE `tb_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `mobile` varchar(20) NOT NULL COMMENT '手机号',
  `password` varchar(64) DEFAULT NULL COMMENT '密码',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='用户';

/*Data for the table `tb_user` */

insert  into `tb_user`(`user_id`,`username`,`mobile`,`password`,`create_time`) values (1,'mark','13612345678','8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918','2017-03-23 22:37:41');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
