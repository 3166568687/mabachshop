package cn.mabach.oauth2.constant;

public interface RedisConstant {
    public static final String KAPTCHA_KEY = "KAPTCHA_KEY_IMAGE_CODE";
    public static final String MOBILE_KEY = "MOBILE_KEY_MOBILE_CODE";
}
