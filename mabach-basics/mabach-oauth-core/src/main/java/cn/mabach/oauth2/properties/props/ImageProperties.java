package cn.mabach.oauth2.properties.props;

import lombok.Data;

@Data
public class ImageProperties {

	private int width = 110;
	private int height = 36;
	private int fontSize=28;
	private int expireIn = 60;

	private String url="/login";
}