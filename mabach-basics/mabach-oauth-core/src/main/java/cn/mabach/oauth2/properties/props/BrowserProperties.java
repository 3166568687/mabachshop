/**
 * 
 */
package cn.mabach.oauth2.properties.props;

import lombok.Data;

/**
 * @author zhailiang
 *
 */
@Data
public class BrowserProperties {
	
	private String loginPage = "/toLogin";
	
	private LoginType loginType = LoginType.REDIRECT;

	private int rememberMeSeconds = 60*60*24*7;


	
}
