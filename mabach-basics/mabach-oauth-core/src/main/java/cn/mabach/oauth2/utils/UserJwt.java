package cn.mabach.oauth2.utils;


import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;



@Data
public class  UserJwt implements UserDetails {

    private static final long serialVersionUID = 4872628781561412463L;

    private static final String ENABLE = "1";

    private Long id;    //用户ID
    private String socialProvider;
    private String nickname;  //用户昵称
    private String icon; //用户头像
    private String openId;
    private String username;
    private String password;
    private String status;
    private Collection<GrantedAuthority> authorities;


    public UserJwt(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UserJwt(String username, String password, Collection<GrantedAuthority> authorities) {
        this.username = username;
        this.password = password;
        this.authorities = authorities;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return !StringUtils.equals(this.status, ENABLE);
    }




}





