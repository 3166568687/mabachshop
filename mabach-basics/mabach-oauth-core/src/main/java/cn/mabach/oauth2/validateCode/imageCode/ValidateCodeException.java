package cn.mabach.oauth2.validateCode.imageCode;

import org.springframework.security.core.AuthenticationException;

/**
 * 自定义验证码异常
 */
public class ValidateCodeException extends AuthenticationException {
    public ValidateCodeException(String msg, Throwable t) {
        super(msg, t);
    }

    public ValidateCodeException(String msg) {
        super(msg);
    }
}