package cn.mabach.oauth2.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.JdbcAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import javax.sql.DataSource;


@Configuration
@EnableAuthorizationServer // 开启了认证服务器
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {


    @Autowired
    private DataSource dataSource;
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired // 刷新令牌
    private UserDetailsService customUserDetailsService;

    @Autowired // 在TokenConfig类中已对添加到容器中了
    private TokenStore tokenStore;

    @Autowired // jwt转换器
    private JwtAccessTokenConverter jwtAccessTokenConverter;



//    ------------------------------------------------------------------
    @Bean
    public ClientDetailsService jdbcClientDetailsService() {

        return new JdbcClientDetailsService(dataSource);
    }

    /**
     * 配置认证服务器
     * 2. 数据库方式
     * @param clients
     * @throws Exception
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {

        clients.withClientDetails(jdbcClientDetailsService());


    }
    //    ------------------------------------------------------------------


    /**
     * 将授权码存到数据库中，在授权码被使用后会被删除
     * @return
     */
    @Bean
    public AuthorizationCodeServices jdbcAuthorizationCodeServices() {
        return new JdbcAuthorizationCodeServices(dataSource);
    }


    //    ------------------------------------------------------------------

    /**
     * 认证服务器整合 端点配置
     * @param endpoints
     * @throws Exception
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        // password模式 要这个 AuthenticationManager 实例
        endpoints.authenticationManager(authenticationManager);
        // 刷新令牌时需要使用
        endpoints.userDetailsService(customUserDetailsService);
        // 整合令牌的管理方式
        endpoints.tokenStore(tokenStore)
                .accessTokenConverter(jwtAccessTokenConverter);

        // 授权码管理策略 会产生的授权码放到 oauth_code 表中，如果这个授权码已经使用了，则对应这个表中的数据就会被删除
        endpoints.authorizationCodeServices(jdbcAuthorizationCodeServices());
    }
    //    ------------------------------------------------------------------



    /**
     * 令牌端点的安全配置
     * @param security
     * @throws Exception
     */
//    @Override
//    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
//        // 认证后可访问 /oauth/token_key , 默认拒绝访问
//        security.tokenKeyAccess("permitAll()");
//        // 认证后可访问 /oauth/check_token , 默认拒绝访问
//        security.checkTokenAccess("isAuthenticated()");
//    }
}
