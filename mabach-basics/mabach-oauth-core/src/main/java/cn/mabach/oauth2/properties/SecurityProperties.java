package cn.mabach.oauth2.properties;

import cn.mabach.oauth2.properties.props.CodeProperties;
import cn.mabach.oauth2.properties.props.Oauth2Properties;
import cn.mabach.oauth2.properties.props.SocialProperties;
import cn.mabach.oauth2.properties.props.BrowserProperties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@ConfigurationProperties(prefix = "mabach.security")
@Data
@Configuration
public class SecurityProperties {
	
	private BrowserProperties browser = new BrowserProperties();

	private CodeProperties code = new CodeProperties();

	private SocialProperties social=new SocialProperties();
	private Oauth2Properties oauth2=new Oauth2Properties();

}