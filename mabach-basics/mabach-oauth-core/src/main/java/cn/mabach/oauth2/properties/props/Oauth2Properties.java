package cn.mabach.oauth2.properties.props;

import lombok.Data;

/**
 * @Author: ming
 * @Date: 2020/2/6 0006 下午 8:30
 */
@Data
public class Oauth2Properties {
    private String clientId="mabach";
    private String clientSecret="123456";
    private String scope="all";

}
