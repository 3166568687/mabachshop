package cn.mabach.oauth2.qq;

import lombok.Data;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @Author: ming
 * @Date: 2020/2/2 0002 下午 7:35
 */
@Data
public class QQAuthenticationProvider implements AuthenticationProvider {

    private UserDetailsService userDetailsService;
//    调用UserDetailsService认证用户，重新返回封装权限后的Authentication
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        QQAuthenticationToken qqAuthenticationTokenBefore=(QQAuthenticationToken)authentication;
        UserDetails userDetails = userDetailsService.loadUserByUsername((String) qqAuthenticationTokenBefore.getPrincipal());
        if (userDetails == null) {
            throw new InternalAuthenticationServiceException("无法获取用户信息");
        }
//认证通过后重新封装封装带权限的构造方法
        QQAuthenticationToken qqauthenticationTokenAfter=new QQAuthenticationToken(userDetails.getUsername(), userDetails.getAuthorities());
        qqauthenticationTokenAfter.setDetails(userDetails);
        return qqauthenticationTokenAfter;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return QQAuthenticationToken.class.isAssignableFrom(aClass);
    }
}
