/**
 * 
 */
package cn.mabach.oauth2.properties.props;

/**
 * @author ming
 *
 */
public enum LoginType {
	
	REDIRECT,
	
	JSON

}
