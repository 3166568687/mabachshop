/**
 * 
 */
package cn.mabach.oauth2.properties.props;

import lombok.Data;

/**
 * @author zhailiang
 *
 */
@Data
public class SmsCodeProperties {
	
	private int length = 6;
	private int expireIn = 60*10;
	private String url="/login/mobile";

}
