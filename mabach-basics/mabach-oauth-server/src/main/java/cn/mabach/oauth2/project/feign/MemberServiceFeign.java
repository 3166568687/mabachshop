package cn.mabach.oauth2.project.feign;

import cn.mabach.member.service.MemberService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-member")
public interface MemberServiceFeign extends MemberService {
}
