package cn.mabach.order.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class ReceiverInfoParam implements Serializable {
    private Long orderId;
    private String receiverName;
    private String receiverPhone;
    private String receiverPostCode;
    private String receiverDetailAddress;
    private String receiverProvince;
    private String receiverCity;
    private String receiverRegion;
    private Integer status;
}