package cn.mabach.order.dto;

import cn.mabach.order.entity.CompanyAddressEntity;
import cn.mabach.order.entity.OrderReturnApplyEntity;
import lombok.Data;

@Data
public class OrderReturnApplyResult extends OrderReturnApplyEntity {
    private CompanyAddressEntity companyAddressEntity;
}
