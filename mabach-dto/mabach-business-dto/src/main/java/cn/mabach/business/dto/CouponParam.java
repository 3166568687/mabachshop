package cn.mabach.business.dto;

import cn.mabach.business.entity.CouponEntity;
import cn.mabach.business.entity.CouponProductCategoryRelationEntity;
import cn.mabach.business.entity.CouponProductRelationEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class CouponParam extends CouponEntity {
    @ApiModelProperty(value = "优惠券绑定的商品")
    private List<CouponProductRelationEntity> productRelationList;
    @ApiModelProperty(value = "优惠券绑定的商品分类")
    private List<CouponProductCategoryRelationEntity> productCategoryRelationList;

}
