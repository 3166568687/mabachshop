package cn.mabach.goods.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ProductResult extends  ProductParam{
    @ApiModelProperty("商品分类的父分类id")
    private Long cateParentId;
}
