package cn.mabach.member.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 用户和标签关系表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-03 23:11:59
 */
@Data
@TableName("ums_member_member_tag_relation")
public class MemberMemberTagRelationEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
		@TableId
		private Long id;
	/**
	 * 
	 */
		private Long memberId;
	/**
	 * 
	 */
		private Long tagId;

}
