package cn.mabach.member.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-02-24 21:58:59
 */
@Data
@TableName("my_friend")
public class MyFriendEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
		@TableId
	
private Long id;
	/**
	 * 自己的用户ID
	 */
	
private Long myUserId;
	/**
	 * 朋友的用户ID
	 */
	
private Long friendUserId;

}
