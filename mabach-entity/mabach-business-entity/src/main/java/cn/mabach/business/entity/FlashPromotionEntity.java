package cn.mabach.business.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;


import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 限时购表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-02 19:26:19
 */
@Data
@TableName("sms_flash_promotion")
public class FlashPromotionEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
		@TableId
		private Long id;
	/**
	 * 
	 */
		private String title;
	/**
	 * 开始日期
	 */

	private Date startDate;
	/**
	 * 结束日期
	 */

	private Date endDate;
	/**
	 * 上下线状态
	 */
		private Integer status;
	/**
	 * 秒杀时间段名称
	 */

	private Date createTime;

}
