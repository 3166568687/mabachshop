package cn.mabach.business.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 优惠券和产品分类关系表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-02 19:26:19
 */
@Data
@TableName("sms_coupon_product_category_relation")
public class CouponProductCategoryRelationEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
		@TableId
		private Long id;
	/**
	 * 
	 */
		private Long couponId;
	/**
	 * 
	 */
		private Long productCategoryId;
	/**
	 * 产品分类名称
	 */
		private String productCategoryName;
	/**
	 * 父分类名称
	 */
		private String parentCategoryName;

}
