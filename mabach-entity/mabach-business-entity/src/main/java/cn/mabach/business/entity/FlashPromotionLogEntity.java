package cn.mabach.business.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;


import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 限时购通知记录
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-02 19:26:19
 */
@Data
@TableName("sms_flash_promotion_log")
public class FlashPromotionLogEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
		@TableId
		private Integer id;
	/**
	 * 
	 */
		private Integer memberId;
	/**
	 * 
	 */
		private Long productId;
	/**
	 * 
	 */
		private String memberPhone;
	/**
	 * 
	 */
		private String productName;
	/**
	 * 会员订阅时间
	 */

	private Date subscribeTime;
	/**
	 * 
	 */

	private Date sendTime;

}
