package cn.mabach.cms.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 话题分类表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-28 16:25:19
 */
@Data
@TableName("cms_topic_category")
public class TopicCategoryEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
		@TableId
		private Long id;
	/**
	 * 
	 */
		private String name;
	/**
	 * 分类图标
	 */
		private String icon;
	/**
	 * 专题数量
	 */
		private Integer subjectCount;
	/**
	 * 
	 */
		private Integer showStatus;
	/**
	 * 
	 */
		private Integer sort;

}
