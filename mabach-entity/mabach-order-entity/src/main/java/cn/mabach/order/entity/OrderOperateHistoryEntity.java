package cn.mabach.order.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;


import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 订单操作历史记录
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-02 23:46:11
 */
@Data
@TableName("oms_order_operate_history")
public class OrderOperateHistoryEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
		@TableId
		private Long id;
	/**
	 * 订单id
	 */
		private Long orderId;
	/**
	 * 操作人：用户；系统；后台管理员
	 */
		private String operateMan;
	/**
	 * 操作时间
	 */
//	@JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
//	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
	/**
	 * 订单状态：0->待付款；1->待发货；2->已发货；3->已完成；4->已关闭；5->无效订单
	 */
		private Integer orderStatus;
	/**
	 * 备注
	 */
		private String note;

}
