package cn.mabach.goods.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 相册表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-28 16:29:32
 */
@Data
@TableName("tb_album")
public class AlbumEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
		@TableId
		private Long id;
	/**
	 * 
	 */
		private String name;
	/**
	 * 
	 */
		private String coverPic;
	/**
	 * 
	 */
		private Integer picCount;
	/**
	 * 
	 */
		private Integer sort;
	/**
	 * 
	 */
		private String description;

}
