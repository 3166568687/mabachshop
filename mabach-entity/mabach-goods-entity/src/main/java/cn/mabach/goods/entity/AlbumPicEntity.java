package cn.mabach.goods.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 画册图片表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-28 16:29:32
 */
@Data
@TableName("tb_album_pic")
public class AlbumPicEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
		@TableId
		private Long id;
	/**
	 * 
	 */
		private Long albumId;
	/**
	 * 
	 */
		private String pic;

}
