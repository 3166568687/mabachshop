package cn.mabach.goods.dao;


import cn.mabach.goods.entity.ProductAttributeCategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 产品属性分类表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-25 22:23:55
 */
@Mapper
public interface ProductAttributeCategoryDao extends BaseMapper<ProductAttributeCategoryEntity> {
	
}
