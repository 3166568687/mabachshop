package cn.mabach.goods.dao;


import cn.mabach.goods.entity.SkuStockEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * sku的库存
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-25 22:23:55
 */
@Mapper
public interface SkuStockDao extends BaseMapper<SkuStockEntity> {
	
}
