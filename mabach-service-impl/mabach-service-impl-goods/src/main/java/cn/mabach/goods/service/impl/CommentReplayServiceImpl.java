package cn.mabach.goods.service.impl;


import cn.mabach.goods.dao.CommentReplayDao;
import cn.mabach.goods.entity.CommentReplayEntity;
import cn.mabach.goods.service.CommentReplayService;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
public class CommentReplayServiceImpl extends ServiceImpl<CommentReplayDao, CommentReplayEntity> implements CommentReplayService {

    @Override
    public RS<PageResult<CommentReplayEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                         @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                         @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        if (pageSize>20){
            return RS.error("CommentReplayServiceImpl非法参数");
        }

        IPage<CommentReplayEntity> page = this.page(
                new Page<CommentReplayEntity>(pageNum,pageSize),
                new QueryWrapper<CommentReplayEntity>().like("name",keyword)
        );

        log.info("CommentReplayServiceImpl:{}",Thread.currentThread().getName());
        return RS.ok(new PageResult(page));
    }




    @Override
    public RS<CommentReplayEntity> getByIdE(@RequestParam("id") Long id) {
            CommentReplayEntity res = this.getById(id);

        return RS.ok(res);
    }

    @Override
    public RS saveE(@RequestBody  CommentReplayEntity entity) {
        boolean b = this.save(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS updateByIdE(@RequestBody CommentReplayEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }


    @Override
    public RS removeOneById(@RequestParam("id") Long id) {
        boolean b = this.removeById(id);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS removeByIdsE(@RequestBody List<Long> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

}