package cn.mabach.goods.dao;


import cn.mabach.goods.entity.ProductAttributeValueEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 存储产品参数信息的表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-25 22:23:55
 */
@Mapper
public interface ProductAttributeValueDao extends BaseMapper<ProductAttributeValueEntity> {
	
}
