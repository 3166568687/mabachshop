package cn.mabach.dianping.dao;


import cn.mabach.dianping.entity.ShopEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-02-28 17:58:07
 */
@Mapper
public interface ShopDao extends BaseMapper<ShopEntity> {
	
}
