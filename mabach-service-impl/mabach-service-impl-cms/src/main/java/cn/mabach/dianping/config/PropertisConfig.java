package cn.mabach.dianping.config;


import cn.mabach.dianping.propertie.CategoryProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: ming
 * @Date: 2020/3/2 0002 下午 3:05
 */

@Configuration
@EnableConfigurationProperties(CategoryProperties.class)
public class PropertisConfig {
}
