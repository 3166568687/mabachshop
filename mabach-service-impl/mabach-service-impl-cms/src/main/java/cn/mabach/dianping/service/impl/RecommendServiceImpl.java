package cn.mabach.dianping.service.impl;

import cn.mabach.dianping.dao.RecommendDao;
import cn.mabach.dianping.entity.RecommendEntity;
import cn.mabach.dianping.service.RecommendService;
import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class RecommendServiceImpl extends ServiceImpl<RecommendDao, RecommendEntity> implements RecommendService {

    @Override
    public PageResult queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        IPage<RecommendEntity> page = this.page(
                new Page<RecommendEntity>(pageNum,pageSize),
                new QueryWrapper<RecommendEntity>().like(!StringUtils.isEmpty(keyword),"name",keyword)
        );

        return new PageResult(page);
    }

    @Override
    public RecommendEntity getByIdE(Integer id) {
        return this.getById(id);
    }

    @Override
    public RS saveE(RecommendEntity entity) {
        boolean b = this.save(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();

    }

    @Override
    public RS updateByIdE(RecommendEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS removeOneById(Integer id) {
        boolean b = this.removeById(id);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    @Override
    public RS removeByIdsE(List<Integer> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

}