package cn.mabach.dianping.mongoDb;

import cn.mabach.dianping.entity.ShopEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

/**
 * @Author: ming
 * @Date: 2020/3/17 0017 下午 5:08
 */
@Service
public interface ShopMongo extends MongoRepository<MongoShopEntity,Integer> {
}
