package cn.mabach.dianping.kafka;

import lombok.Data;

/**
 * @Author: ming
 * @Date: 2020/3/24 0024 下午 8:08
 */

public interface KafkaProperties {
     String zk="120.79.17.187:2181";
     String topic="topic1";
     String broker="120.79.17.187:9092";
}
