package cn.mabach.dianping.propertie;

import lombok.Data;

import java.util.List;

/**
 * @Author: ming
 * @Date: 2020/3/2 0002 下午 3:18
 */
@Data
public class ListProperties {
    private Integer id;
    private List<String> names;
}
