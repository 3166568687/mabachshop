package cn.mabach.dianping.kafka;



import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;

import java.util.Properties;

/**
 * @Author: ming
 * @Date: 2020/3/24 0024 下午 8:03
 */
public class KafkaProducer {
    private String topic;
    private static Producer<Integer,Object> producer;

    public KafkaProducer(String topic){
        this.topic=topic;
        Properties properties = new Properties();
        properties.put("metadata.broker.list", KafkaProperties.broker);
        properties.put("serializer.class","kafka.serializer.StringEncoder");
        properties.put("request.required.ack","1");

        producer=new Producer<Integer,Object>(new ProducerConfig(properties));
    }


    public  static void sendMessage(String topic,Object o){

        producer.send(new KeyedMessage<Integer, Object>(topic,o));
    }
}
