package cn.mabach.business.dao;


import cn.mabach.business.entity.HomeAdvertiseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 首页轮播广告表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 17:41:08
 */
@Mapper
public interface HomeAdvertiseDao extends BaseMapper<HomeAdvertiseEntity> {
	
}
