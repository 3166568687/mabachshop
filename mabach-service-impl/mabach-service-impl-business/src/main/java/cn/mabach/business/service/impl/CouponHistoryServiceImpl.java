package cn.mabach.business.service.impl;



import cn.mabach.business.entity.CouponHistoryEntity;
import cn.mabach.business.dao.CouponHistoryDao;
import cn.mabach.business.service.CouponHistoryService;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class CouponHistoryServiceImpl extends ServiceImpl<CouponHistoryDao, CouponHistoryEntity> implements CouponHistoryService {

    /*
     *分页查询
     * */
    @Override
    public RS<PageResult<CouponHistoryEntity>> queryPage(@RequestParam(value = "couponId", required = false) Long couponId,
                                                         @RequestParam(value = "useStatus", required = false) Integer useStatus,
                                                         @RequestParam(value = "orderSn", required = false) String orderSn,
                                                         @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                         @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {


        IPage<CouponHistoryEntity> page = this.page(
                new Page<CouponHistoryEntity>(pageNum,pageSize),
                new QueryWrapper<CouponHistoryEntity>().eq(couponId!=null,"coupon_id",couponId)
                .eq(useStatus!=null,"use_status",useStatus)
                .eq(!StringUtils.isEmpty(orderSn),"order_sn",orderSn)
        );

        return RS.ok(new PageResult(page));
    }



    /*
     *根据id查询
     * */
    @Override
    public RS<CouponHistoryEntity> getByIdE(@RequestParam("id") Long id) {
            CouponHistoryEntity res = this.getById(id);

        return RS.ok(res);
    }

    @Override
    public RS saveE(@RequestBody  CouponHistoryEntity entity) {
        boolean b = this.save(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id更改
     * */
    @Override
    public RS updateByIdE(@RequestBody CouponHistoryEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id集合删除
     * */
    @Override
    @Transactional
    public RS removeByIdsE(@RequestParam("ids") List<Long> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }



}