package cn.mabach.business.service.impl;



import cn.mabach.business.dao.CouponDao;
import cn.mabach.business.dto.CouponParam;
import cn.mabach.business.entity.CouponEntity;
import cn.mabach.business.entity.CouponProductCategoryRelationEntity;
import cn.mabach.business.entity.CouponProductRelationEntity;
import cn.mabach.business.service.CouponProductCategoryRelationService;
import cn.mabach.business.service.CouponProductRelationService;
import cn.mabach.business.service.CouponService;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import cn.mabach.utils.BeanUtilsMabach;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class CouponServiceImpl extends ServiceImpl<CouponDao, CouponEntity> implements CouponService {


    @Autowired
    private CouponProductRelationService couponProductRelationService;
    @Autowired
    private CouponProductCategoryRelationService couponProductCategoryRelationService;
    /*
     *分页查询
     * */
    @Override
    public RS<PageResult<CouponEntity>> queryPage(@RequestParam(value = "name",required = false) String name,
                                                  @RequestParam(value = "type",required = false) Integer type,
                                                  @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                  @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {


        IPage<CouponEntity> page = this.page(
                new Page<CouponEntity>(pageNum,pageSize),
                new QueryWrapper<CouponEntity>().like(!StringUtils.isEmpty(name),"name",name)
                .eq(type!=null,"type",type)
        );

        return RS.ok(new PageResult(page));
    }



    /*
     *根据id查询
     * */
    @Override
    public RS<CouponParam> getByIdE(@RequestParam("id") Long id) {
            CouponEntity res = this.getById(id);
        CouponParam couponParam = BeanUtilsMabach.doToDto(res, CouponParam.class);
//根据优惠券id查找关联产品关系表
        RS<List<CouponProductRelationEntity>> byCouponId = couponProductRelationService.getByCouponId(id);

        //根据优惠券id查找关联类目关系表
        RS<List<CouponProductCategoryRelationEntity>> byCouponId1 = couponProductCategoryRelationService.getByCouponId(id);
//        关联产品关系
        couponParam.setProductRelationList(byCouponId.getData());
//        关联类目关系
        couponParam.setProductCategoryRelationList(byCouponId1.getData());

        return RS.ok(couponParam);
    }

    @Override
    @Transactional
    public RS saveE(@RequestBody CouponParam entity) {
        Boolean flag=false;

        CouponEntity couponEntity = BeanUtilsMabach.doToDto(entity, CouponEntity.class);
        couponEntity.setUseCount(0);
        couponEntity.setReceiveCount(0);
//        保存优惠券实体
        this.save(couponEntity);
//插入优惠券和商品关系表
        if(entity.getUseType()==2){

            for (CouponProductRelationEntity couponProductRelationEntity : entity.getProductRelationList()) {
                couponProductRelationEntity.setCouponId(entity.getId());
                couponProductRelationService.saveE(couponProductRelationEntity);
            }
        }
//插入优惠券和商品分类关系表
        if (entity.getUseType()==1){

            for (CouponProductCategoryRelationEntity couponProductCategoryRelationEntity : entity.getProductCategoryRelationList()) {
                couponProductCategoryRelationEntity.setCouponId(entity.getId());
                couponProductCategoryRelationService.saveE(couponProductCategoryRelationEntity);
            }
        }

        flag=true;
        if (flag){
            return RS.ok();
        }
        return RS.error();
    }

    /*
     *根据id更改
     * */
    @Override
    @Transactional
    public RS updateByIdE(@RequestBody CouponParam entity) {
        Boolean flag=false;

        CouponEntity couponEntity = BeanUtilsMabach.doToDto(entity, CouponEntity.class);
        this.updateById(couponEntity);

        //先删除后插入优惠券和商品关系表
        if (entity.getUseType()==2){
            couponProductRelationService.deleteByCouponId(entity.getId());
            for (CouponProductRelationEntity couponProductRelationEntity : entity.getProductRelationList()) {
                couponProductRelationEntity.setCouponId(entity.getId());
                couponProductRelationService.saveE(couponProductRelationEntity);

            }
        }


        if (entity.getUseType()==1){
            couponProductCategoryRelationService.deleteByCouponId(entity.getId());
            for (CouponProductCategoryRelationEntity relationEntity : entity.getProductCategoryRelationList()) {
                relationEntity.setCouponId(entity.getId());
                couponProductCategoryRelationService.saveE(relationEntity);
            }
        }
        flag=true;
        if (flag){
            return RS.ok();
        }
        return RS.error();
    }

    /*
     *根据id集合删除
     * */
    @Override
    @Transactional
    public RS removeByIdsE(@RequestParam("ids") List<Long> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    public RS removeByIdE(@RequestParam("id") Long id){
        boolean b = this.removeById(id);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

}