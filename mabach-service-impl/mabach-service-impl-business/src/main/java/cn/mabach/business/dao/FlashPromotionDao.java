package cn.mabach.business.dao;


import cn.mabach.business.entity.FlashPromotionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 限时购表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 17:40:52
 */
@Mapper
public interface FlashPromotionDao extends BaseMapper<FlashPromotionEntity> {
	
}
