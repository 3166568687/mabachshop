package cn.mabach.business.service.impl;



import cn.mabach.business.dao.CouponProductRelationDao;
import cn.mabach.business.entity.CouponProductRelationEntity;
import cn.mabach.business.service.CouponProductRelationService;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class CouponProductRelationServiceImpl extends ServiceImpl<CouponProductRelationDao, CouponProductRelationEntity> implements CouponProductRelationService {

    /*
     *分页查询
     * */
    @Override
    public RS<PageResult<CouponProductRelationEntity>> queryPage(@RequestParam(value = "keyword", required = false) String keyword,
                                                                 @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                                 @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {


        IPage<CouponProductRelationEntity> page = this.page(
                new Page<CouponProductRelationEntity>(pageNum,pageSize),
                new QueryWrapper<CouponProductRelationEntity>().like(!StringUtils.isEmpty(keyword),"name",keyword)
        );

        return RS.ok(new PageResult(page));
    }

    @Override
    public RS deleteByCouponId(Long id) {
        boolean b = this.remove(new QueryWrapper<CouponProductRelationEntity>().eq("coupon_id", id));
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }


    /*
     *根据id查询
     * */
    @Override
    public RS<CouponProductRelationEntity> getByIdE(@RequestParam("id") Long id) {
            CouponProductRelationEntity res = this.getById(id);

        return RS.ok(res);
    }

    @Override
    public RS<List<CouponProductRelationEntity>> getByCouponId(Long id) {
        List<CouponProductRelationEntity> coupon_id = this.list(new QueryWrapper<CouponProductRelationEntity>().eq("coupon_id", id));

        return RS.ok(coupon_id);
    }

    @Override
    public RS saveE(@RequestBody  CouponProductRelationEntity entity) {
        boolean b = this.save(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id更改
     * */
    @Override
    public RS updateByIdE(@RequestBody CouponProductRelationEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id集合删除
     * */
    @Override
    @Transactional
    public RS removeByIdsE(@RequestParam("ids") List<Long> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }



}