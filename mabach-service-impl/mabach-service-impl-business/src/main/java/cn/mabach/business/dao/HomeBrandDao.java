package cn.mabach.business.dao;


import cn.mabach.business.entity.HomeBrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 首页推荐品牌表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 17:41:08
 */
@Mapper
public interface HomeBrandDao extends BaseMapper<HomeBrandEntity> {
	
}
