package cn.mabach.business.service.impl;



import cn.mabach.business.dao.HomeNewProductDao;
import cn.mabach.business.entity.HomeNewProductEntity;
import cn.mabach.business.service.HomeNewProductService;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class HomeNewProductServiceImpl extends ServiceImpl<HomeNewProductDao, HomeNewProductEntity> implements HomeNewProductService {

    /*
     *分页查询
     * */
    @Override
    public RS<PageResult<HomeNewProductEntity>> queryPage(@RequestParam(value = "productName", required = false) String productName,
                                                          @RequestParam(value = "recommendStatus", required = false) Integer recommendStatus,
                                                          @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                          @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {


        IPage<HomeNewProductEntity> page = this.page(
                new Page<HomeNewProductEntity>(pageNum,pageSize),
                new QueryWrapper<HomeNewProductEntity>().like(!StringUtils.isEmpty(productName),"product_name",productName)
                .eq(recommendStatus!=null,"recommend_status",recommendStatus)
        );

        return RS.ok(new PageResult(page));
    }



    /*
     *根据id查询
     * */
    @Override
    public RS<HomeNewProductEntity> getByIdE(@RequestParam("id") Long id) {
            HomeNewProductEntity res = this.getById(id);

        return RS.ok(res);
    }

    @Override
    public RS saveE(@RequestBody  HomeNewProductEntity entity) {
        boolean b = this.save(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id更改
     * */
    @Override
    public RS updateByIdE(@RequestBody HomeNewProductEntity entity) {
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id集合删除
     * */
    @Override
    @Transactional
    public RS removeByIdsE(@RequestParam("ids") List<Long> ids) {
        boolean b = this.removeByIds(ids);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     *根据id删除
     * */
    public RS removeByIdE(@RequestParam("id") Long id){
        boolean b = this.removeById(id);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }

    /*
     * 修改推荐排序
     * */
    @Override
    public RS updateSort(@RequestParam("id") Long id, @RequestParam("sort") Integer sort) {
        HomeNewProductEntity entity = new HomeNewProductEntity();
        entity.setId(id);
        entity.setSort(sort);
        boolean b = this.updateById(entity);
        if (!b){
            return RS.error();
        }
        return RS.ok();
    }


    /*
     * 批量修改推荐排序
     * */
    @Override
    public RS updateRecommendStatus(@RequestParam("ids") List<Long> ids, @RequestParam Integer recommendStatus) {
        for (Long id : ids) {
            HomeNewProductEntity entity = new HomeNewProductEntity();
            entity.setId(id);
            entity.setRecommendStatus(recommendStatus);
            boolean b = this.updateById(entity);
            if (!b){
                return RS.error();
            }
        }
        return RS.ok();
    }

}