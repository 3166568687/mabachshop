package cn.mabach.business.dao;


import cn.mabach.business.entity.FlashPromotionSessionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 限时购场次表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 17:41:08
 */
@Mapper
public interface FlashPromotionSessionDao extends BaseMapper<FlashPromotionSessionEntity> {
	
}
