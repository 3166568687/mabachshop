package cn.mabach.member.dao;


import cn.mabach.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


/**
 * 会员表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-04 14:32:44
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {

    @Select("SELECT resource  FROM perms p LEFT JOIN role_perms rp ON p.id=rp.perm_id LEFT JOIN role r ON rp.role_id=r.id LEFT JOIN member_role mr ON r.id=mr.role_id LEFT JOIN ums_member m ON mr.member_id=m.id WHERE username =#{username}")
	List<String> findPermsByUsername(@Param("username") String username);
}
