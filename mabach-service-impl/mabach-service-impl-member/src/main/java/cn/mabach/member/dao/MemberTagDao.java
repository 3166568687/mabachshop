package cn.mabach.member.dao;


import cn.mabach.member.entity.MemberTagEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 用户标签表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-04 17:30:47
 */
@Mapper
public interface MemberTagDao extends BaseMapper<MemberTagEntity> {
	
}
