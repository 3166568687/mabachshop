package cn.mabach.search.dao;


import cn.mabach.search.entity.SkuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 商品表
 * 
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-02-17 17:59:18
 */
@Mapper
public interface SkuDao extends BaseMapper<SkuEntity> {
	
}
