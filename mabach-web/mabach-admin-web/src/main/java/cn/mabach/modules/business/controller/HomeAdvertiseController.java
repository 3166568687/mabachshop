package cn.mabach.modules.business.controller;

import cn.mabach.business.entity.HomeAdvertiseEntity;
import cn.mabach.modules.business.feign.HomeAdvertiseServiceFeign;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 首页轮播广告表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-31 21:38:07
 */
@RestController
@RequestMapping("/home/advertise")
public class HomeAdvertiseController {
    @Autowired
    private HomeAdvertiseServiceFeign homeAdvertiseService;



    /**
        * 分页列表
        */
    @GetMapping(value = "/list")
    public PageResult<HomeAdvertiseEntity> getList(@RequestParam(value = "name", required = false) String name,
                                                   @RequestParam(value = "type", required = false) Integer type,
                                                   @RequestParam(value = "endTime", required = false) String endTime,
                                                   @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                   @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        RS<PageResult<HomeAdvertiseEntity>> pageResultRS = homeAdvertiseService.queryPage(name, type,endTime,pageNum, pageSize);

        return pageResultRS.getData();
    }



    /**
     * 根据ID获取信息
     */

    @GetMapping(value = "/{id}")
    public HomeAdvertiseEntity getItem(@PathVariable("id") Long id) {
        return homeAdvertiseService.getByIdE(id).getData();
    }





    /**
     * 根据id保存
     */
    @PostMapping(value = "/create")
    public RS create(@RequestBody HomeAdvertiseEntity homeAdvertiseEntity) {

        RS rs = homeAdvertiseService.saveE(homeAdvertiseEntity);

        return rs;
    }



    /**
     * 根据id修改
     */
    @PostMapping(value = "/update/{id}")
    public RS update(@PathVariable("id") Long id,
                     @Validated @RequestBody HomeAdvertiseEntity entity,
                     BindingResult result) {
        entity.setId(id);
        RS rs = homeAdvertiseService.updateByIdE(entity);

        return rs;
    }

    /**
       * 根据id集合删除
       */
    @PostMapping(value = "/delete")
    public RS deleteBatch(@RequestParam("ids") List<Long> ids) {
        RS rs = homeAdvertiseService.removeByIdsE(ids);
        return rs;
    }

    /**
         * 根据id删除
         */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public RS removeByIdE(@PathVariable Long id) {

        return homeAdvertiseService.removeByIdE(id);

    }

    /*
    * 修改上下线状态
    * */
    @RequestMapping(value = "/update/status", method = RequestMethod.POST)
    public RS updateStatus(@RequestParam("id") Long id, @RequestParam("status") Integer status) {
       return homeAdvertiseService.updateStatus(id,status);
    }
}
