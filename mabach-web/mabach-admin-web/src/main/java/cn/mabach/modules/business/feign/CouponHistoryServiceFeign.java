package cn.mabach.modules.business.feign;

import cn.mabach.business.service.CouponHistoryService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-business")
public interface CouponHistoryServiceFeign extends CouponHistoryService {
}
