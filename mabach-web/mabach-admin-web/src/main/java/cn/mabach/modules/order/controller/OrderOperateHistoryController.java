package cn.mabach.modules.order.controller;


import cn.mabach.order.entity.OrderOperateHistoryEntity;
import cn.mabach.order.service.OrderOperateHistoryService;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 订单操作历史记录
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-02 23:46:11
 */
@RestController
@RequestMapping("/orderoperatehistory")
public class OrderOperateHistoryController {
    @Autowired
    private OrderOperateHistoryService orderOperateHistoryService;



    /**
        * 分页列表
        */
    @GetMapping(value = "/list")
    public PageResult<OrderOperateHistoryEntity> getList(@RequestParam(value = "keyword", required = false) String keyword,
                                                         @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                         @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        RS<PageResult<OrderOperateHistoryEntity>> pageResultRS = orderOperateHistoryService.queryPage(keyword, pageNum, pageSize);

        return pageResultRS.getData();
    }



    /**
     * 根据ID获取信息
     */

    @GetMapping(value = "/{id}")
    public OrderOperateHistoryEntity getItem(@PathVariable("id") Long id) {
        return orderOperateHistoryService.getByIdE(id).getData();
    }





    /**
     * 根据id保存
     */
    @PostMapping(value = "/create")
    public RS create(@RequestBody OrderOperateHistoryEntity orderOperateHistoryEntity) {

        RS rs = orderOperateHistoryService.saveE(orderOperateHistoryEntity);

        return rs;
    }



    /**
     * 根据id修改
     */
    @PostMapping(value = "/update/{id}")
    public RS update(@PathVariable("id") Long id,
                     @Validated @RequestBody OrderOperateHistoryEntity entity,
                     BindingResult result) {
        entity.setId(id);
        RS rs = orderOperateHistoryService.updateByIdE(entity);

        return rs;
    }

    /**
       * 根据id集合删除
       */
    @PostMapping(value = "/delete")
    public RS deleteBatch(@RequestParam("ids") List<Long> ids) {
        RS rs = orderOperateHistoryService.removeByIdsE(ids);
        return rs;
    }

    /**
         * 根据id删除
         */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public RS removeByIdE(@PathVariable Long id) {

        return orderOperateHistoryService.removeByIdE(id);

    }


}
