package cn.mabach.modules.dianping.feign;

import cn.mabach.dianping.service.ShopService;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @Author: ming
 * @Date: 2020/2/28 0028 上午 12:53
 */
@FeignClient(name = "app-mabach-dianping")
public interface ShopServiceFeign extends ShopService {
}
