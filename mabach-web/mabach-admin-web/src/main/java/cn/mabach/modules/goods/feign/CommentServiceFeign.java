package cn.mabach.modules.goods.feign;

import cn.mabach.goods.service.CommentService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-goods")
public interface CommentServiceFeign extends CommentService {
}
