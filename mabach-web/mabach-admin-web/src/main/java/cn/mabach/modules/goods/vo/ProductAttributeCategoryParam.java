package cn.mabach.modules.goods.vo;

import lombok.Data;

@Data
public class ProductAttributeCategoryParam {
    Long id;
    String name;
}
