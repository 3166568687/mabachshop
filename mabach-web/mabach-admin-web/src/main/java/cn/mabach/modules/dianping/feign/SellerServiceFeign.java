package cn.mabach.modules.dianping.feign;

import cn.mabach.dianping.service.SellerService;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @Author: ming
 * @Date: 2020/2/28 0028 上午 12:48
 */
@FeignClient(name = "app-mabach-dianping")
public interface SellerServiceFeign extends SellerService {
}
