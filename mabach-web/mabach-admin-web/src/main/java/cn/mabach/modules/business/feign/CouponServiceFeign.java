package cn.mabach.modules.business.feign;

import cn.mabach.business.service.CouponService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-business")
public interface CouponServiceFeign extends CouponService {
}
