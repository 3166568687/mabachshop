package cn.mabach.modules.goods.controller;

import cn.mabach.goods.dto.ProductAttributeCategoryItem;
import cn.mabach.goods.entity.ProductAttributeCategoryEntity;
import cn.mabach.modules.goods.feign.ProductAttributeCategoryServiceFeign;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 产品属性分类表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-26 01:13:22
 */
@RestController
@RequestMapping("/productAttribute/category")
@Slf4j
public class ProductAttributeCategoryController {
    @Autowired
    private ProductAttributeCategoryServiceFeign productAttributeCategoryService;

    /**
     * 列表
     */


    @GetMapping(value = "/list")
    public PageResult<ProductAttributeCategoryEntity> getList(
                                                                  @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                                  @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        RS<PageResult<ProductAttributeCategoryEntity>> pageResultRS = productAttributeCategoryService.queryPage(pageNum, pageSize);
        log.info("ProductAttributeCategoryController:{},id:{}",Thread.currentThread().getName(),Thread.currentThread().getId());

        return pageResultRS.getData();
    }



    /**
     * 信息
     */

    @GetMapping(value = "/{id}")
    public ProductAttributeCategoryEntity getItem(@PathVariable("id") Long id) {
        return productAttributeCategoryService.getByIdE(id).getData();
    }





    /**
     * 保存
     */
    @PostMapping(value = "/create")
    public RS create(@RequestParam("name") String name) {
        ProductAttributeCategoryEntity productAttributeCategoryEntity = new ProductAttributeCategoryEntity();
        productAttributeCategoryEntity.setName(name);
        productAttributeCategoryEntity.setAttributeCount(0);
        productAttributeCategoryEntity.setParamCount(0);

        RS rs = productAttributeCategoryService.saveE(productAttributeCategoryEntity);

        return rs;
    }



    /**
     * 修改
     */
    @PostMapping(value = "/update/{id}")
    public RS update(@PathVariable("id") Long id,
                     @RequestParam("name") String name) {
        ProductAttributeCategoryEntity entity = new ProductAttributeCategoryEntity();

        entity.setId(id);
        entity.setName(name);

        RS rs = productAttributeCategoryService.updateByIdE(entity);

        return rs;
    }




    /**
     * 删除
     */
    @GetMapping(value = "/delete/{id}")
    public RS delete(@PathVariable("id") Long id) {
        RS rs = productAttributeCategoryService.removeOneById(id);

        return rs;

    }


    @PostMapping(value = "/delete/batch")
    public RS deleteBatch(@RequestParam("ids") List<Long> ids) {
        RS rs = productAttributeCategoryService.removeByIdsE(ids);
        return rs;
    }


    @GetMapping(value = "/list/withAttr")
    public List<ProductAttributeCategoryItem> getListWithAttr() {

        return productAttributeCategoryService.getListWithAttr().getData();
    }


}
