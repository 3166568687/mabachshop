package cn.mabach.modules.goods.vo;

import lombok.Data;

@Data
public class UpdateShowStatusParam {
    Long[] ids;
    Integer showStatus;
}
