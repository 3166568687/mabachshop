package cn.mabach.modules.business.feign;

import cn.mabach.business.service.HomeNewProductService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-business")
public interface HomeNewProductServiceFeign extends HomeNewProductService {
}
