package cn.mabach.modules.goods.feign;

import cn.mabach.goods.service.FeightTemplateService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-goods")
public interface FeightTemplateServiceFeign extends FeightTemplateService {
}
