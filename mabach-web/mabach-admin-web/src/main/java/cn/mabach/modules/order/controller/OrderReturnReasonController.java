package cn.mabach.modules.order.controller;


import cn.mabach.order.entity.OrderReturnReasonEntity;
import cn.mabach.order.service.OrderReturnReasonService;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 退货原因表
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2020-01-02 23:46:11
 */
@RestController
@RequestMapping("/returnReason")
public class OrderReturnReasonController {
    @Autowired
    private OrderReturnReasonService orderReturnReasonService;



    /**
        * 分页列表
        */
    @GetMapping(value = "/list")
    public PageResult<OrderReturnReasonEntity> getList(@RequestParam(value = "keyword", required = false) String keyword,
                                                       @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                       @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        RS<PageResult<OrderReturnReasonEntity>> pageResultRS = orderReturnReasonService.queryPage(keyword, pageNum, pageSize);

        return pageResultRS.getData();
    }



    /**
     * 根据ID获取信息
     */

    @GetMapping(value = "/{id}")
    public OrderReturnReasonEntity getItem(@PathVariable("id") Long id) {
        return orderReturnReasonService.getByIdE(id).getData();
    }





    /**
     * 根据id保存
     */
    @PostMapping(value = "/create")
    public RS create(@RequestBody OrderReturnReasonEntity orderReturnReasonEntity) {

        RS rs = orderReturnReasonService.saveE(orderReturnReasonEntity);

        return rs;
    }



    /**
     * 根据id修改
     */
    @PostMapping(value = "/update/{id}")
    public RS update(@PathVariable("id") Long id,
                     @Validated @RequestBody OrderReturnReasonEntity entity,
                     BindingResult result) {
        entity.setId(id);
        RS rs = orderReturnReasonService.updateByIdE(entity);

        return rs;
    }

    /**
       * 根据id集合删除
       */
    @PostMapping(value = "/delete")
    public RS deleteBatch(@RequestParam("ids") List<Long> ids) {
        RS rs = orderReturnReasonService.removeByIdsE(ids);
        return rs;
    }

    /**
         * 根据id删除
         */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public RS removeByIdE(@PathVariable Long id) {

        return orderReturnReasonService.removeByIdE(id);

    }

    /*
     * 修改退货原因启用状态
     * */
    @RequestMapping(value = "/update/status", method = RequestMethod.POST)
    public RS updateStatus(@RequestParam(value = "status") Integer status,
                           @RequestParam("ids") List<Long> ids) {
        return orderReturnReasonService.updateStatus(status,ids);
    }

}
