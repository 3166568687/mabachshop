package cn.mabach.modules.goods.feign;

import cn.mabach.goods.service.MemberPriceService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-goods")
public interface MemberPriceServiceFeign extends MemberPriceService {
}
