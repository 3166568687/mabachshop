package cn.mabach.modules.goods.feign;

import cn.mabach.goods.service.ProductCategoryAttributeRelationService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-goods")
public interface ProductCategoryAttributeRelationServiceFeign extends ProductCategoryAttributeRelationService {
}
