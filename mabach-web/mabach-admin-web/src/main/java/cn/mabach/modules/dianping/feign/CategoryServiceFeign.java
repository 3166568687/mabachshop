package cn.mabach.modules.dianping.feign;

import cn.mabach.dianping.service.CategoryService;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @Author: ming
 * @Date: 2020/2/28 0028 上午 12:52
 */
@FeignClient(name = "app-mabach-dianping")
public interface CategoryServiceFeign extends CategoryService {
}
