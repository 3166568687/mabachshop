package cn.mabach.modules.goods.controller;


import cn.mabach.goods.entity.SkuStockEntity;
import cn.mabach.modules.goods.feign.SkuStockServiceFeign;

import cn.mabach.result.PageResult;
import cn.mabach.result.RS;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * sku的库存
 *
 * @author chenshun
 * @email 176468159@qq.com
 * @date 2019-12-29 02:40:20
 */
@RestController
@RequestMapping("/sku")
@Slf4j
public class SkuStockController {
    @Autowired
    private SkuStockServiceFeign skuStockService;



    /**
        * 分页列表
        */
    @GetMapping(value = "/list")
    public PageResult<SkuStockEntity> getList(@RequestParam(value = "keyword", required = false) String keyword,
                                              @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                              @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize) {

        RS<PageResult<SkuStockEntity>> pageResultRS = skuStockService.queryPage(keyword, pageNum, pageSize);

        return pageResultRS.getData();
    }







    /**
     * 保存
     */
    @PostMapping(value = "/create")
    public RS create(@RequestBody SkuStockEntity skuStockEntity) {

        RS rs = skuStockService.saveE(skuStockEntity);

        return rs;
    }



    /*
     * 根据商品编号及编码模糊搜索sku库存
     * */

    @RequestMapping(value = "/{pid}", method = RequestMethod.GET)
    public List<SkuStockEntity> getList(@PathVariable("pid") Long pid, @RequestParam(value = "keyword",required = false) String keyword){
        RS<List<SkuStockEntity>> list = skuStockService.getList(pid, keyword);
        return list.getData();
    }


    @RequestMapping(value ="/update/{pid}",method = RequestMethod.POST)
    public RS update(@PathVariable("pid") Long pid,@RequestBody List<SkuStockEntity> skuStockList){
       return skuStockService.update(pid,skuStockList);
    }




}
