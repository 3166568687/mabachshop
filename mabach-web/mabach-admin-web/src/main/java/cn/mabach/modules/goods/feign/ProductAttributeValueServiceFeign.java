package cn.mabach.modules.goods.feign;

import cn.mabach.goods.service.ProductAttributeValueService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-goods")
public interface ProductAttributeValueServiceFeign extends ProductAttributeValueService {
}
