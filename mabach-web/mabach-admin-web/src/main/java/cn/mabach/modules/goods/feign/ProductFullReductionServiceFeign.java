package cn.mabach.modules.goods.feign;

import cn.mabach.goods.service.ProductFullReductionService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-goods")
public interface ProductFullReductionServiceFeign extends ProductFullReductionService {
}
