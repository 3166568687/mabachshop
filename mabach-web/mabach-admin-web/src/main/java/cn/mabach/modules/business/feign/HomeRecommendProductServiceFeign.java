package cn.mabach.modules.business.feign;

import cn.mabach.business.service.HomeRecommendProductService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-business")
public interface HomeRecommendProductServiceFeign extends HomeRecommendProductService {
}
