package cn.mabach.portal.config;

import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.net.InetSocketAddress;

/**
 * @Author: ming
 * @Date: 2020/2/24 0024 下午 10:53
 */
@Component
@Slf4j
public class NettyServer {
    private static final int port=9410;

    @PostConstruct
    public void run()  {
        log.info("开始加载NettyServer");
        EventLoopGroup boss = new NioEventLoopGroup();
        EventLoopGroup work = new NioEventLoopGroup();
        ServerBootstrap sb = new ServerBootstrap();
        sb.group(boss,work).channel(NioServerSocketChannel.class)

                .childHandler(new WSServerInitialzer());
        log.info("开始阻塞");
        ChannelFuture future = sb.bind(port);
        if (future.isSuccess()){
            log.info("启动成功");
        }
    }
}
