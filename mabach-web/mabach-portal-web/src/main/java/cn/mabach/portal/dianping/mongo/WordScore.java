package cn.mabach.portal.dianping.mongo;

import lombok.Data;

/**
 * @Author: ming
 * @Date: 2020/3/26 0026 下午 9:24
 */
@Data
public class WordScore {
    private Integer _1;
    private Double _2;
}
