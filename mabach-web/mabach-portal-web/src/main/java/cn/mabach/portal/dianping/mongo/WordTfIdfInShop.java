package cn.mabach.portal.dianping.mongo;

import lombok.Data;

/**
 * @Author: ming
 * @Date: 2020/3/29 0029 下午 3:51
 */
@Data
public class WordTfIdfInShop {
    private Integer sid;
    private Double score;
}
