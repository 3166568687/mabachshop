package cn.mabach.portal.feign;

import cn.mabach.member.service.MemberService;
import cn.mabach.search.service.SkuService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-search")
public interface SkuServiceFeign extends SkuService {
}
