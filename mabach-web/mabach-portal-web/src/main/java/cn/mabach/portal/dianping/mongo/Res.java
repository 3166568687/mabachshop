package cn.mabach.portal.dianping.mongo;

import lombok.Data;

import java.util.List;

/**
 * @Author: ming
 * @Date: 2020/3/25 0025 下午 2:27
 */
@Data
public class Res {
    private Integer _id;
    private List<Recs> recs;

}
