package cn.mabach.portal.dianping.mongo;

import io.swagger.models.auth.In;
import lombok.Data;
import scala.Int;

import java.util.List;

/**
 * @Author: ming
 * @Date: 2020/3/29 0029 下午 3:52
 */
@Data
public class WordInshop {
    private Integer _id;
    private List<WordTfIdfInShop> wordTfIdfInShop;
}
