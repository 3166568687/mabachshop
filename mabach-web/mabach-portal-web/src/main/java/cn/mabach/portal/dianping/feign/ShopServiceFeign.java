package cn.mabach.portal.dianping.feign;

import cn.mabach.dianping.service.ShopService;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @Author: ming
 * @Date: 2020/2/29 0029 下午 10:38
 */
@FeignClient(name = "app-mabach-dianping")
public interface ShopServiceFeign extends ShopService {
}
