package cn.mabach.portal.kafka.mongoEntity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: ming
 * @Date: 2020/3/26 0026 下午 8:35
 */
@Data
public class ShopWeight implements Serializable {
    private Integer sid;
    private Double weight;
    private Long date;

}
